import { useNavigation } from '@react-navigation/native';
import React, { useContext, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { Avatar, Box, Divider, Queue, Spacing, Text } from '../../components';
import {
  DocumentIcon,
  LogOut,
  ProfileIcon,
  RightArrowIcon,
} from '../../components/icons';
import { useDocument } from '../../hooks';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { AuthContext } from '../../provider/auth';
import storage from '@react-native-firebase/storage';
import { WelcomeNestScreen } from './welcome-nest';

export const ProfileScreen = () => {
  const { user, signOut } = useContext(AuthContext);
  const { doc } = useDocument(`users/${user?.uid}`);
  const { firstName, lastName }: any = doc || {};
  const navigation = useNavigation();

  const [avatarURL, setAvatarURL] = useState(null);

  useEffect(() => {
    if (user) {
      storage()
        .ref(`/avatar-images/${user?.uid}`)
        .getDownloadURL()
        .then((res: any) => setAvatarURL(res));
    }
  }, [user]);

  if (!user) {
    return <WelcomeNestScreen />;
  }

  return (
    <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
      <Box height="100%" width="100%" role="fawhite">
        <Box role="white" height={60} justifyContent="flex-start">
          <Spacing mh={5} mv={5}>
            <Queue justifyContent="space-between">
              <Box width={32} height={32}></Box>
              <Spacing mt={1}>
                <Text type="title3" textAlign="center" role="black100" bold>
                  Profile
                </Text>
              </Spacing>
              <TouchableOpacity onPress={() => signOut()}>
                <LogOut />
              </TouchableOpacity>
            </Queue>
          </Spacing>
        </Box>
        <Box flex={1} alignItems="center" justifyContent="flex-end">
          <Spacing mt={8} mb={4}>
            <Avatar
              size="L"
              source={{ uri: avatarURL }}
              initial={`${lastName && lastName[0]}${firstName && firstName[0]}`}
            />
          </Spacing>
          <Text type="title3" role="black" textAlign="center" bold>
            {firstName ? `${lastName && lastName[0]}.${firstName}` : 'user'}
          </Text>
        </Box>
        <Box flex={2}>
          <Spacing mt={3} pt={8}>
            <Divider />
            <TouchableOpacity
              onPress={() =>
                navigation.navigate(NavigationRoutes.PersonalInformation)
              }
            >
              <Box height={64} justifyContent="center">
                <Spacing mh={5}>
                  <Queue justifyContent="space-between">
                    <Queue>
                      <ProfileIcon role="black" width={24} height={24} />
                      <Spacing ml={4}>
                        <Text type="body" role="black">
                          Хувийн мэдээлэл
                        </Text>
                      </Spacing>
                    </Queue>
                    <Spacing mt={1}>
                      <RightArrowIcon role="black" width={8} height={14} />
                    </Spacing>
                  </Queue>
                </Spacing>
              </Box>
            </TouchableOpacity>
          </Spacing>
          <Divider />
          <TouchableOpacity
            onPress={() => navigation.navigate(NavigationRoutes.Admission)}
          >
            <Box height={64} justifyContent="center">
              <Spacing mh={5}>
                <Queue justifyContent="space-between">
                  <Queue>
                    <DocumentIcon />
                    <Spacing ml={4}>
                      <Text type="body" role="black">
                        Элсэлтийн явц
                      </Text>
                    </Spacing>
                  </Queue>
                  <Spacing mt={1}>
                    <RightArrowIcon role="black" width={8} height={14} />
                  </Spacing>
                </Queue>
              </Spacing>
            </Box>
          </TouchableOpacity>
          <Divider />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

import React, { useContext, useEffect, useState } from 'react';

import {
  Box,
  Stack,
  Text,
  Spacing,
  Border,
  Button,
  LozengeStatus,
  AdmissionProcessCard,
  AdmissionProcessCardHeader,
  AdmissionProcessCardContent,
  CheckIconTwo,
} from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useDocument } from '../../hooks';
import { AuthContext } from '../../provider/auth';
import { useQuery } from '@apollo/client';
import { REQUEST_ADMISSION_DATA } from '../../graphql';
import _ from 'lodash';
import { useCollection } from '../../hooks/firebase';
import { FlatList } from 'react-native';

const StartButton: React.FC<any> = ({ authstatus, title }) => {
  const navigation = useNavigation();

  const route = (): any => {
    switch (title) {
      case 'Бүртгэл':
        return NavigationRoutes.MainCourseRegistration;
      case 'Шалгалт':
        return NavigationRoutes.ExamStack;
      case 'Ярилцлага':
        return NavigationRoutes.BookMeeting;
    }
  };

  return (
    <Spacing ml={4} mb={3} mr={4} mt={4}>
      <Button
        status={authstatus === 'pending' ? 'disabled' : 'default'}
        width={'100%'}
        onPress={() => navigation.navigate(route())}
      >
        <Text
          fontFamily={'Montserrat'}
          role={'white'}
          type={'callout'}
          textAlign={'center'}
          bold
        >
          Эхлэх
        </Text>
      </Button>
    </Spacing>
  );
};

const AdmissionProcess: React.FC<any> = ({ children, programId }) => {
  return (
    <Stack size={3}>
      {React.Children.toArray(children).map((child: any, index) => {
        return React.cloneElement(child, { programId });
      })}
    </Stack>
  );
};

const Step: React.FC<any> = ({
  authstep,
  authstatus,
  stepNumber,
  children,
  title,
}) => {
  return (
    <Box width={'100%'} height={'auto'} flexDirection={'row'}>
      <Box flex={1} alignItems={'flex-start'}>
        <Box flex={1} alignItems={'center'}>
          <CircleStep
            stepNumber={stepNumber}
            authstep={authstep}
            statusP={authstatus}
          />
          <VerticalLine stepNumber={stepNumber} authstep={authstep} />
        </Box>
      </Box>
      <Box flex={5}>
        <AdmissionProcessCard>
          <AdmissionProcessCardHeader>
            <Spacing ph={3} pv={3}>
              {(stepNumber < authstep && (
                <LozengeStatus
                  type={'success'}
                  style={'subtle'}
                  status={'АМЖИЛТТАЙ'}
                />
              )) ||
                (stepNumber === authstep && authstatus === 'error' && (
                  <LozengeStatus
                    type={'error'}
                    style={'subtle'}
                    status={'АМЖИЛТГҮЙ'}
                  />
                )) ||
                (stepNumber === authstep && authstatus === 'pending' && (
                  <LozengeStatus
                    type={'pending'}
                    style={'subtle'}
                    status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                  />
                )) ||
                (stepNumber === authstep && authstatus === 'default' && (
                  <LozengeStatus
                    type={'default'}
                    style={'subtle'}
                    status={'DEFAULT'}
                  />
                ))}
              <Text
                bold
                type={'headline'}
                fontFamily={'Montserrat'}
                role={'black'}
              >
                {title}
              </Text>
            </Spacing>
          </AdmissionProcessCardHeader>
          <AdmissionProcessCardContent>
            <Spacing p={3}>{children}</Spacing>
          </AdmissionProcessCardContent>
          {stepNumber === authstep && authstatus !== 'error' && (
            <StartButton
              authstatus={authstatus}
              stepNumber={stepNumber}
              title={title}
            />
          )}
        </AdmissionProcessCard>
      </Box>
    </Box>
  );
};

const CircleStep: React.FC<any> = ({ stepNumber, authstep, statusP }) => {
  return (
    <Box height={32} width={32}>
      <Border
        radius={16}
        lineWidth={1}
        role={(stepNumber <= authstep && 'success500') || 'black'}
      >
        <Box
          height={'100%'}
          width={'100%'}
          role={(stepNumber <= authstep && 'success200') || 'white'}
          alignItems={'center'}
          justifyContent={'center'}
        >
          {statusP === 'success' ? (
            <CheckIconTwo height={20} width={20}></CheckIconTwo>
          ) : (
            <Text>{stepNumber}</Text>
          )}
        </Box>
      </Border>
    </Box>
  );
};

const VerticalLine: React.FC<any> = ({ authstep, stepNumber }) => {
  return (
    <Box flex={1} zIndex={2} width={0} opacity={1}>
      <Spacing mt={2}>
        <Border
          type={'dashed'}
          lineWidth={1}
          role={stepNumber < authstep ? 'success300' : 'gray'}
        >
          <Box height={'100%'} width={0}></Box>
        </Border>
      </Spacing>
    </Box>
  );
};

export const AdmissionScreen = () => {
  const { data } = useQuery(REQUEST_ADMISSION_DATA);
  const { user } = useContext(AuthContext);
  const { doc }: any = useDocument(`users/${user?.uid}`);
  const [admissionStep, setAdmissionStep] = useState(1);
  const [admissionStatus, setAdmissionStatus] = useState(null);
  const { collection }: any = useCollection(`users/${user?.uid}/admissions`);
  const admissionProcessData: any = _.intersectionBy(
    collection,
    data?.mainCourseCollection.items,
    'admissionId'
  )[0];
  const course =
    data &&
    data?.mainCourseCollection.items[
      _.findIndex(data?.mainCourseCollection.items, {
        courseTitle: admissionProcessData?.age < 18 ? 'Hop' : 'Leap',
      })
    ];

  useEffect(() => {
    if (admissionProcessData !== [] && admissionProcessData) {
      setAdmissionStep(admissionProcessData.admissionStep);
      setAdmissionStatus(admissionProcessData.admissionStatus);
    }
  }, [admissionProcessData]);

  return (
    <Box flex={1} role={'fawhite'}>
      <Spacing ml={4} mt={4} mr={4}>
        <AdmissionProcess programId={course?.admissionId}>
          <FlatList
            style={{ height: '150%' }}
            data={course?.admissionProcess}
            renderItem={({ item, index }) => (
              <Step
                title={item.title}
                authstep={admissionStep}
                authstatus={admissionStatus}
                stepNumber={index + 1}
              >
                <Spacing mb={2}>
                  <Text type={'footnote'} role={'primary500'}>
                    {item.description}
                  </Text>
                </Spacing>
              </Step>
            )}
            keyExtractor={(_, index) => 'admissionStep-' + index}
            ItemSeparatorComponent={() => <Box height={12} />}
          />
        </AdmissionProcess>
      </Spacing>
    </Box>
  );
};

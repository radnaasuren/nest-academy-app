import React from 'react';
import { View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import {
  Text,
  Box,
  Spacing,
  CloseLineIcon,
  Button,
  Queue,
  Stack,
  Border,
} from '../../components';
import { EnableNotificationIllustration } from '../../components/illustration';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const EnableNotificationScreen: React.FC<ModalType> = ({
  navigation,
  route,
}) => {
  const { title, description } = route.params || {};
  const content =
    'Та элсэлт болон бүртгэлтэй холбоотой мэдээллийг цаг алдалгүй авахын тулд notification-оо идэвхжүүлнэ үү.';

  return (
    <Box flex={1} justifyContent={'center'} alignItems={'center'}>
      <TouchableOpacity
        style={StyleSheet.absoluteFill}
        onPress={() => navigation.goBack()}
      >
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(18,18,18,0.4)' },
          ]}
        ></View>
      </TouchableOpacity>
      <Border radius={8}>
        <Box width={Dimensions.get('window').width * 0.9} role={'primary100'}>
          <Spacing mt={5} mr={5}>
            <Queue justifyContent={'flex-end'}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <CloseLineIcon />
              </TouchableOpacity>
            </Queue>
          </Spacing>
          <Spacing mt={15} mb={15} mr={10} ml={10}>
            <Stack size={6} justifyContent={'center'} alignItems={'center'}>
              <EnableNotificationIllustration />
              <Spacing mb={5} />
              <Text
                type={'title1'}
                bold
                fontFamily={'Montserrat'}
                textAlign={'center'}
              >
                {title || 'Notification'}
              </Text>
              <Text type={'body'} textAlign={'center'} width={250}>
                {description || content}
              </Text>
              <Queue justifyContent={'center'}>
                <Button
                  width={200}
                  onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
                  status="default"
                  size={'s'}
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    ИДЭВХЖҮҮЛЭХ
                  </Text>
                </Button>
              </Queue>
              <Queue justifyContent={'center'}>
                <Button
                  onPress={() => navigation.goBack()}
                  category="text"
                  size="s"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'primary500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Алгасах
                  </Text>
                </Button>
              </Queue>
            </Stack>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};
const styles = StyleSheet.create({
  img: {
    width: 168,
    height: 158,
  },
});

type ModalType = {
  route: any;
  navigation: any;
};

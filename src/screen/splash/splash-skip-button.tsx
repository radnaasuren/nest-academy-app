import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { Box, Button, Text } from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const SplashSkipButton = (): any => {
  const navigation = useNavigation();

  return (
    <Box flex={1} position={'absolute'} top={40} right={10} zIndex={5}>
      <Button
        category={'text'}
        size={'s'}
        status={'active'}
        onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
        width={95}
      >
        <Text fontFamily={'Montserrat'} bold type={'body'}>
          АЛГАСАХ
        </Text>
      </Button>
    </Box>
  );
};

import React from 'react';
import { useNavigation } from '@react-navigation/native';
import {
  Text,
  Button,
  Spacing,
  Box,
  BackgroundImage,
  Stack,
} from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const SplashThird = () => {
  const navigation = useNavigation();

  return (
    <Box flex={1} alignItems="center">
      <Spacing mt={31.5}>
        <Stack size={6} alignItems={'center'}>
          <Spacing mb={8}>
            <Box height={347}>
              <BackgroundImage
                source={require('../../assets/images/splash-third.png')}
                height={350}
                width={173}
              />
            </Box>
          </Spacing>
          <Text type={'title3'} bold textAlign={'center'} role={'primary500'}>
            Өөрийгөө сорь
          </Text>
          <Text
            type={'body'}
            textAlign={'center'}
            role={'gray'}
            width={316}
            bold
          >
            Та шинэ зүйлсийг туршиж үзэж өөрийгөө сорих боломжтой
          </Text>
        </Stack>
      </Spacing>

      <Box position={'absolute'} bottom={40}>
        <Button
          type={'primary'}
          category={'fill'}
          onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
          width={296}
        >
          <Text
            fontFamily={'Montserrat'}
            bold
            type={'headline'}
            role={'fawhite'}
          >
            ЭХЭЛЦГЭЭЕ
          </Text>
        </Button>
      </Box>
    </Box>
  );
};

import React, { useState } from 'react';
import { SafeAreaView } from 'react-native';
import Swiper from 'react-native-swiper';

import { Border, Box, Queue } from '../../components';
import { SplashFirst } from './splash-first';
import { SplashSecond } from './splash-second';
import { SplashSkipButton } from './splash-skip-button';
import { SplashThird } from './splash-third';

const Indicator = ({ cur, index }: any) => {
  return (
    <Border radius={5}>
      <Box
        width={6}
        height={6}
        role={cur === index ? 'darkgray' : 'lightgray'}
      />
    </Border>
  );
};

export const SplashScreen = () => {
  const [cur, setCur] = useState(0);

  return (
    <SafeAreaView
      style={{ flex: 1, backgroundColor: '#FFF', alignItems: 'center' }}
    >
      <SplashSkipButton />

      <Swiper
        showsPagination={false}
        loop={false}
        onIndexChanged={(res) => setCur(res)}
      >
        <SplashFirst />
        <SplashSecond />
        <SplashThird />
      </Swiper>

      <Box
        position={'absolute'}
        bottom={88}
        alignItems={'center'}
        display={cur === 2 ? 'none' : 'flex'}
      >
        <Queue size={2.5}>
          <Indicator cur={cur} index={0} />
          <Indicator cur={cur} index={1} />
          <Indicator cur={cur} index={2} />
        </Queue>
      </Box>
    </SafeAreaView>
  );
};

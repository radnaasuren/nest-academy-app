import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Route,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { Text } from '../../components/core/text';
import { Button } from '../../components/core/button';
import { Border } from '../../components/core';

export const MicroCoursePopupScreen: React.FC<ModalType> = ({
  navigation,
  route,
}) => {
  const { title, description, icon, onClick } = route.params || {};
  const content =
    'Та манай аппликэшинд бүртгүүлснээр цааш илүү их хичээлүүдийг үзэх боломжмой болно. ';

  return (
    <Box flex={1} justifyContent={'center'} alignItems={'center'}>
      <TouchableOpacity
        style={StyleSheet.absoluteFill}
        onPress={() => navigation.goBack()}
      >
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(18,18,18,0.4)' },
          ]}
        ></View>
      </TouchableOpacity>
      <Border radius={8}>
        <Box width={Dimensions.get('window').width * 0.9} role={'primary100'}>
          <Spacing mt={15} mb={15} mr={10} ml={10}>
            <Stack size={6}>
              <Queue justifyContent={'center'}>
                <Image
                  source={require('../../assets/images/Lock.png')}
                  style={styles.img}
                />
              </Queue>
              <Text
                type={'title1'}
                bold
                fontFamily={'Montserrat'}
                textAlign={'center'}
              >
                {title || 'Ooppss !!'}
              </Text>
              <Text
                fontFamily={'Montserrat'}
                type={'body'}
                textAlign={'center'}
              >
                {description || content}
              </Text>
              <Queue justifyContent={'center'}>
                <Button width={200} onPress={onClick} status="default">
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Бүртгүүлэх
                  </Text>
                </Button>
              </Queue>
              <Queue justifyContent={'center'}>
                <Button
                  onPress={() => console.log('handled')}
                  category="text"
                  size="s"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'primary500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                    underline
                  >
                    Нэвтрэх
                  </Text>
                </Button>
              </Queue>
            </Stack>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};
const styles = StyleSheet.create({
  img: {
    width: 168,
    height: 158,
  },
});

type ModalType = {
  route: any;
  navigation: any;
};

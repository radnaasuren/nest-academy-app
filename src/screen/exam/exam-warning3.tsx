import React from 'react';
import {
  Box,
  Warning,
  CancelIllustration,
  TopNavigation,
  Spacing,
  Text,
} from '../../components';
import { useNavigation } from '@react-navigation/native';

export const ExamWarningScreen3 = () => {
  const navigation = useNavigation();
  return (
    <Box flex={1} justifyContent="space-evenly" role="white">
      <Box alignSelf="center">
        <CancelIllustration />
      </Box>
      <Spacing mh={4}>
        <Warning
          onPress={() => {
            navigation.navigate('ExamWarning4');
          }}
          title="Санамж #3"
          buttonname="Дараах"
        >
          <Text role="primary500" type="body" textAlign="center" width={275}>
            Та шалгалтаас гарсан тохиолдолд таны оноо гүйцэтгэсэн ажлаар
            дүгнэгдэх болно.
          </Text>
        </Warning>
      </Spacing>
    </Box>
  );
};

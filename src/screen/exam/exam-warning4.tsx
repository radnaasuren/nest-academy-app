import React, { useState } from 'react';
import {
  Box,
  Warning,
  LaunchIllustration,
  Text,
  TimeIcon,
  QuestionIcon,
  WarnIconBG,
  PopUp,
  Spacing,
  Button,
  RightArrowIcon
} from '../../components';
import { useNavigation } from '@react-navigation/native';
export const ExamWarningScreen4 = () => {
  const [popUp, setPopUp] = useState(false)
  const navigation = useNavigation();
  return (
    <Box
      flex={1}
      alignItems="center"
      justifyContent="space-evenly"
      role="white"
    >
      <LaunchIllustration />
      <Spacing mh={4}>
        <Warning
          onPress={() => {
            setPopUp(true)
          }}
          title="Шалгалтаа эхлэх"
          titleAlign="left"
          buttonname="Эхлэх"
          twobutton={false}
        >
          <Box
            width="100%"
            height={60}
            justifyContent="center"
            alignItems="center"
          >
            <Box
              height={60}
              width="80%"
              flexDirection="row"
              justifyContent="space-between"
            >
              <Box height={60} justifyContent="space-between" width={100}>
                <Text type="subheading" role="black">
                  Хугацаа
              </Text>
                <Box flexDirection="row" height={20} justifyContent="center">
                  <Box width={30}>
                    <TimeIcon />
                  </Box>
                  <Text type="subheading" role="primary500" bold={true}>
                    30 минут
                </Text>
                </Box>
              </Box>
              <Box flex={1} justifyContent="center" alignItems="center">
                <Box width={1} height="100%" role="gray" />
              </Box>
              <Box height={60} justifyContent="space-between" width={100}>
                <Text type="subheading" role="black">
                  Асуулт
              </Text>
                <Box flexDirection="row" height={20} justifyContent="center">
                  <Box width={30}>
                    <QuestionIcon />
                  </Box>
                  <Text type="subheading" role="primary500" bold={true}>
                    30 минут
                </Text>
                </Box>
              </Box>
            </Box>
          </Box>
        </Warning>
      </Spacing>
      {
        popUp &&
        <PopUp isVisible={popUp} height={369} Close={() => { setPopUp(false) }} >
          <WarnIconBG />
          <Box width={'100%'} alignItems={'center'} justifyContent={'center'} >
            <Text bold type={'title3'} fontFamily={'Montserrat'} >Баталгаажуулах</Text>
            <Spacing mt={2} >
              <Text width={200} textAlign={'center'} type={'body'} role={'black'} >Та шалгалтаа эхлэхдээ итгэлтэй байна уу?</Text>
            </Spacing>
          </Box>
          <Spacing mt={4} >
            <Button size={'s'} onPress={() => { navigation.navigate('ExamScreen') }} >
              <Box width={100} flexDirection={'row'} justifyContent={'center'} alignItems={'center'} >
                <Spacing mr={2} >
                  <Text bold type={'subheading'} role={'white'} >ЭХЛЭХ</Text>
                </Spacing>
                <RightArrowIcon />
              </Box>
            </Button>
          </Spacing>
        </PopUp>
      }
    </Box>
  );
};

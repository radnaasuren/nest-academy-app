import React, { useContext } from 'react';
import {
  Text,
  Box,
  Button,
  Spacing,
  CountdownTimer,
  PopUp,
  WarningIcon,
  WarnIconBG,
} from '../../components';
import { ExamContext, ExamProvider } from '../../provider/exam-provider';
import { ExamCard } from './exam-card';
import { FlatList } from 'react-native';
import { RightArrowIcon } from '../../components/icons/right-arrow-icon';
import { SafeAreaView } from 'react-native-safe-area-context';
import { HeaderLeft, HeaderRight, HeaderTitle } from '../../navigation/headers';

const CustomExamHeader = () => {
  const { setPopUp, endExam } = useContext(ExamContext);
  return (
    <SafeAreaView style={{ height: 100, backgroundColor: 'white', zIndex: 1 }}>
      <Box
        flexDirection={'row'}
        justifyContent={'space-between'}
        alignItems={'center'}
        role={'white'}
      >
        <HeaderLeft
          onPress={() => {
            setPopUp(true);
          }}
        />
        <Box flexDirection={'row'} width={'auto'}>
          <Spacing mr={4}>
            <CountdownTimer onFinish={endExam} role={'black100'} from={1800} />
          </Spacing>
          <HeaderTitle title={'Шалгалт'} />
        </Box>
        <HeaderRight
          onPress={() => {
            setPopUp(true);
          }}
        />
      </Box>
    </SafeAreaView>
  );
};

const Exam: React.FC<any> = (props) => {
  const { popUp, setPopUp, quizes, finishExam } = useContext(ExamContext);

  return (
    <Box flex={1}>
      <Spacing mr={4} ml={4}>
        <FlatList
          style={{ overflow: 'visible' }}
          data={quizes}
          renderItem={({ item, index }) => {
            return <ExamCard qId={index + 1} {...item} />;
          }}
          ItemSeparatorComponent={() => <Spacing mt={8} />}
          ListHeaderComponent={() => <Spacing mt={8} />}
          ListFooterComponent={() => <Spacing mb={30} />}
          keyExtractor={(item, index) => item.id}
          showsVerticalScrollIndicator={false}
        />
      </Spacing>
      <Box
        width={'100%'}
        height={'auto'}
        position={'absolute'}
        bottom={0}
        role={'lightgray'}
      >
        <Spacing mb={10} ml={4} mr={4}>
          <Button width={'100%'} onPress={() => setPopUp(true)}>
            <Box width={'auto'} flexDirection={'row'} alignItems={'center'}>
              <Text
                fontFamily={'Montserrat'}
                type={'callout'}
                width={'auto'}
                role={'white'}
                bold
              >
                ШАЛГАЛТ ДУУСГАХ
              </Text>
              <Spacing ml={4}>
                <RightArrowIcon />
              </Spacing>
            </Box>
          </Button>
        </Spacing>
      </Box>
      <Box width={100} height={1}></Box>
      {popUp && (
        <PopUp
          isVisible={popUp}
          Close={() => {
            setPopUp(false);
          }}
        >
          <WarnIconBG />
          <Box width={'100%'} alignItems={'center'} justifyContent={'center'}>
            <Text bold type={'title3'} fontFamily={'Montserrat'}>
              Баталгаажуулах
            </Text>
            <Spacing mt={2}>
              <Text
                width={200}
                textAlign={'center'}
                type={'body'}
                role={'black'}
              >
                {'Та шалгалтаа дуусгахаас өмнө хариултаа сайн нягтална уу'}
              </Text>
            </Spacing>
          </Box>
          <Spacing mt={4}>
            <Button size={'s'} onPress={finishExam}>
              <Box
                width={100}
                flexDirection={'row'}
                justifyContent={'center'}
                alignItems={'center'}
              >
                <Spacing>
                  <Text bold type={'subheading'} role={'white'}>
                    {'ДУУСГАХ'}
                  </Text>
                </Spacing>
              </Box>
            </Button>
          </Spacing>
        </PopUp>
      )}
    </Box>
  );
};

export const ExamScreen: React.FC<any> = (props) => {
  return (
    <ExamProvider>
      <CustomExamHeader />
      <Exam />
    </ExamProvider>
  );
};

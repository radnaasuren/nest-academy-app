import React from 'react';
import { FlatList, ScrollView } from 'react-native';
import { Border, Shadow, Text, Button } from '../../components/core';
import {
  Spacing,
  Box,
  Stack,
  Queue,
  BackgroundImage,
} from '../../components/layout';
import { CheckIcon } from '../../components/icons';

const LineOfExperience = ({ text }: any) => {
  return (
    <Queue size={2}>
      <CheckIcon width={24} height={24} role={'accentNest'} />
      <Text type={'body'} role={'black'}>
        {text}
      </Text>
    </Queue>
  );
};

export const FCAboutScreen = (prams: any) => {
  const { teachersCollection, description, knowledge } = prams.route.params;

  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: 'white' }}
      contentContainerStyle={{ alignItems: 'center' }}
    >
      <Spacing pv={6} ph={4}>
        <Stack size={4}>
          <Shadow radius={2} h={0} w={0} opacity={0.25} role={'primary500'}>
            <Border radius={4}>
              <Box role={'white'} height={'auto'} width={'100%'}>
                <Spacing p={4}>
                  <Queue size={4} alignItems={'center'}>
                    <Border radius={32}>
                      <BackgroundImage
                        height={64}
                        width={64}
                        source={{ uri: teachersCollection.items[0].image.url }}
                        resizeMode={'cover'}
                      />
                    </Border>
                    <Stack>
                      <Text type={'subheading'} bold={true} role={'gray'}>
                        {' '}
                        {teachersCollection.items[0].position}{' '}
                      </Text>
                      <Text type={'headline'} bold={true} role={'black'}>
                        {' '}
                        {teachersCollection.items[0].name}{' '}
                      </Text>
                    </Stack>
                  </Queue>
                </Spacing>
              </Box>
            </Border>
          </Shadow>

          <Spacing mv={4}>
            <Box height={1} width={'100%'} role={'offwhite'}></Box>
          </Spacing>

          <Text type={'headline'} bold={true} role={'primary500'}>
            {' '}
            Хичээлиийн танилцуулга{' '}
          </Text>
          <Text type={'body'} role={'black'}>
            {' '}
            {description.json.content[0].content[0].value}{' '}
          </Text>

          <Spacing mv={4}>
            <Box height={1} width={'100%'} role={'offwhite'}></Box>
          </Spacing>

          <Text type={'headline'} bold={true} role={'primary500'}>
            {' '}
            Сургалтаас олж авах мэдлэг{' '}
          </Text>
          <Stack size={2}>
            <FlatList
              data={knowledge}
              renderItem={({ item }) => <LineOfExperience text={item} />}
              keyExtractor={(_, index) => 'knowledge-' + index}
              ItemSeparatorComponent={() => <Box height={8} />}
            />
          </Stack>

          <Spacing mv={4}>
            <Box height={1} width={'100%'} role={'offwhite'}></Box>
          </Spacing>

          <Text type={'headline'} bold={true} role={'primary500'}>
            Хөтөлбөрт хичээлд бүртгүүлэх
          </Text>
          <Text type={'body'} bold={false} role={'black'}>
            Хэрвээ та илүү өргөн хүрээтэй хичээл, 4 жилийн чадварлаг багш нараар
            ахлуулсан хичээлд хамрагдах бол доорх товч дээр дарж өөрийн
            мэдээллээ оруулж бидэнтэй нэгдээрэй.
          </Text>

          <Button onPress={() => {}} width={'100%'}>
            <Text
              fontFamily={'Montserrat'}
              bold={true}
              role={'white'}
              type={'subheading'}
            >
              Бүртгүүлэх
            </Text>
          </Button>
        </Stack>
      </Spacing>
    </ScrollView>
  );
};

import React, { useState } from 'react';
import { Dimensions } from 'react-native';
import {
  BackgroundImage,
  TopBarNavigator,
  TopBarScreen,
  VideoPlayer,
} from '../../components';
import { FCAboutScreen, FCStudyScreen } from './index';
const { width } = Dimensions.get('window');

export const FreeCourseScreen = (prams: any) => {
  const { lessonURL, course } = prams.route.params;
  const [lessonUrl, setLesosnUrl] = useState(
    lessonURL !== undefined ? lessonURL : null
  );

  const Image = () => {
    return (
      <BackgroundImage
        source={require('../../assets/images/free-course-img.png')}
        width={'100%'}
        height={256}
      />
    );
  };
  const Video = () => {
    return <VideoPlayer source={{ uri: lessonUrl }} width={width} />;
  };

  return (
    <TopBarNavigator Header={lessonUrl === null ? Image : Video}>
      <TopBarScreen name={'Тухай'} component={FCAboutScreen} prams={course} />
      <TopBarScreen
        name={'Хичээлүүд'}
        component={FCStudyScreen}
        prams={{ ...course, setLesosnUrl }}
      />
    </TopBarNavigator>
  );
};

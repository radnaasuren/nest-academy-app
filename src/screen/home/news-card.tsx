import React from 'react';
import { TouchableOpacity } from 'react-native';
import { BackgroundImage, Border, Box, Spacing, Text } from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';

interface Props {
  source?: any;
  title?: string;
  date?: string;
  screenName?: string;
  entryId?: string;
}

export const NewsCard: React.FC<Props> = ({
  source,
  title,
  date,
  entryId
}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(NavigationRoutes.News, { entryId });
      }}
    >
      <Border radius={8}>
        <Box width={309} height={270} role={'primary100'}>
          <BackgroundImage width={'100%'} resizeMode={'cover'} height={164} source={source} />
          <Spacing pl={3.5}>
            <Box height={106} width={'100%'} justifyContent={'space-evenly'}>
              <Text width={'auto'} type={'body'} fontFamily={'Montserrat'} bold>
                {title}
              </Text>
              <Text
                width={'auto'}
                type={'subheading'}
                fontFamily={'Montserrat'}
              >
                {date}
              </Text>
            </Box>
          </Spacing>
        </Box>
      </Border>
    </TouchableOpacity>
  );
};

import React from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Border, Box, LoaderComponent, Queue, Spacing, Text } from '../../components';
import { NewsCard } from './news-card';
import { Register } from './register';
import { TryCourse } from './try-course';
import { BackgroundImage } from '../../components/layout/background-image';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useQuery } from '@apollo/client';
import { REQUEST_HOME_SCREEN } from '../../graphql/queries';
import _ from 'lodash';

import { Header } from '../../components/layout/header';

const Title: React.FC<any> = ({
  title1,
  title2,
  title1Role = 'black',
  title2Role = 'black',
}) => {
  return (
    <Queue size={1}>
      <Text type={'headline'} role={title1Role} bold width={'auto'}>
        {title1}
      </Text>
      <Text type={'headline'} role={title2Role} bold width={'auto'}>
        {title2}
      </Text>
    </Queue>
  );
};

const Course = ({ source, entryId }: any) => {
  entryId = `${entryId}`
  // console.log(entryId2, ' entryId')
  const navigation = useNavigation();

  return (
    <Border radius={8}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate(NavigationRoutes.MainCourceScreen, { entryId });
        }}
      >
        <BackgroundImage
          source={{ uri: source }}
          height={167}
          width={167}
        ></BackgroundImage>
      </TouchableOpacity>
    </Border>
  );
};
const Notification = [
  {
    id: 1,
    type: 'info',
    title: 'Мэдэгдэл',
    data: 'Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!',
    banner: 'Мэдээлэл авах',
  },
  {
    id: 2,
    type: 'warning',
    title: 'Мэдэгдэл',
    data: 'Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!',
  },
];

export const HomeScreen: React.FC<any> = () => {
  const { data, loading } = useQuery(REQUEST_HOME_SCREEN);
  const allNews = data?.newsCollection?.items;
  console.log('data ', data?.mainCourseCollection?.items)

  const newsAcademy = _.isArray(allNews)
    ? allNews.filter((item: any) => item.category === 'Nest Academy')
    : [];
  const newsFoundation = _.isArray(allNews)
    ? allNews.filter((item: any) => item.category === 'Nest Foundation')
    : [];
  const newsSolutions = _.isArray(allNews)
    ? allNews.filter((item: any) => item.category === 'Nest Solutions')
    : [];

  return (
    <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
      <Header Notification={Notification} />
      <ScrollView>
        <Register />
        <Spacing ph={4}>
          <Box height={'auto'}>
            <Spacing mv={4}>
              <Title title1={'ҮНДСЭН'} title2={'ХӨТӨЛБӨР'} />
            </Spacing>
            <FlatList
              data={loading ? [_, _] : data?.mainCourseCollection?.items}
              renderItem={({ item }) => loading ? <LoaderComponent width={167} height={167} /> : (
                <Course
                  source={item?.courseLogo?.url}
                  screenName={item.courseTitle + 'Screen'}
                  entryId={item?.sys?.id.toString()}
                />
              )}
              keyExtractor={(_, index) => index + 'course'}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing pl={4} mv={2}>
          <Box height={'auto'}>
            <Spacing mb={4}>
              <Title
                title1={'NEST'}
                title2={'ACADEMY'}
                title2Role={'accentNest'}
              />
            </Spacing>
            <FlatList
              data={loading ? [_, _] : newsAcademy}
              renderItem={({ item }) => loading ? <LoaderComponent width={309} height={270} /> : (
                <NewsCard
                  entryId={item?.sys?.id}
                  source={{ uri: item?.posterImage?.url }}
                  title={item?.title}
                  date={'2020-05-30'}
                  screenName={'News'}
                />
              )}
              keyExtractor={(item, index) => item.sys ? item.sys.id : 'academy-' + index}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing pl={4} mv={2}>
          <Box height={'auto'}>
            <Spacing mb={4}>
              <Title
                title1={'NEST'}
                title2={'FOUNDATION'}
                title2Role={'caution400'}
              />
            </Spacing>
            <FlatList
              data={loading ? [_, _] : newsFoundation}
              renderItem={({ item }) => loading ? <LoaderComponent width={309} height={270} /> : (
                <NewsCard
                  entryId={item?.sys?.id}
                  source={{ uri: item?.posterImage?.url }}
                  title={item.title}
                  date={'2020-05-30'}
                  screenName={'News'}
                />
              )}
              keyExtractor={(item, index) => item.sys ? item.sys.id : 'foundation-' + index}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing pl={4} mv={2}>
          <Box height={'auto'}>
            <Spacing mb={4}>
              <Title
                title1={'NEST'}
                title2={'SOLUTIONS'}
                title2Role={'primary500'}
              />
            </Spacing>
            <FlatList
              data={loading ? [_, _] : newsSolutions}
              renderItem={({ item }) => loading ? <LoaderComponent width={309} height={270} /> : (
                <NewsCard
                  entryId={item?.sys?.id}
                  source={{ uri: item?.posterImage?.url }}
                  title={item.title}
                  date={'2020-05-30'}
                  screenName={'News'}
                />
              )}
              keyExtractor={(item, index) => item.sys ? item.sys.id : 'solution-' + index}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing ph={4} mv={2} mb={10}>
          <TryCourse />
        </Spacing>
      </ScrollView>
    </SafeAreaView>
  );
};

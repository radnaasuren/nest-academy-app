import React from 'react';
import {
  Border,
  Box,
  Button,
  PlayerIcon,
  Spacing,
  Text,
} from '../../components';
import {
  EllipseIcon,
  TriangleIcon,
  RectangleIcon,
} from '../../components/icons/home-screen-icons';

export const TryCourse = () => {
  return (
    <Border radius={8}>
      <Box width={'100%'} height={194} role={'primary100'}>
        <Box width={'auto'} position={'absolute'} left={14} top={23}>
          <Text
            fontFamily={'Montserrat'}
            type={'headline'}
            width={268}
            numberOfLines={2}
            bold
          >
            Манай үнэгүй хичээлийг туршиж үзмээр байна уу ?
          </Text>
        </Box>
        <Box width={'auto'} height={'auto'} position={'absolute'} bottom={0}>
          <EllipseIcon />
        </Box>
        <Box
          width={'auto'}
          height={'auto'}
          position={'absolute'}
          bottom={0}
          left={17}
        >
          <TriangleIcon />
        </Box>
        <Box
          width={'auto'}
          height={'auto'}
          position={'absolute'}
          bottom={0}
          left={105}
        >
          <RectangleIcon />
        </Box>
        <Box
          width={'auto'}
          height={'auto'}
          position={'absolute'}
          bottom={30}
          right={24}
        >
          <Button
            category={'text'}
            onPress={() => console.log('ty')}
            width={'auto'}
          >
            <Box flexDirection={'row'} width={'auto'} alignItems={'center'}>
              <Text width={'auto'} type={'headline'} bold>
                Үзэх
              </Text>
              <Spacing ml={3}>
                <PlayerIcon />
              </Spacing>
            </Box>
          </Button>
        </Box>
      </Box>
    </Border>
  );
};

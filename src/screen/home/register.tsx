import { useQuery } from '@apollo/client';
import { useNavigation } from '@react-navigation/core';
import _ from 'lodash';
import React, { useContext } from 'react';
import { View } from 'react-native';
import { ArrowIcon, Box, Button, Spacing, Text } from '../../components';
import { REQUEST_ADMISSION_DATA } from '../../graphql';
import { useCollection, useDocument } from '../../hooks';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { AuthContext } from '../../provider/auth';
import { ComputerIcon } from '../../components/icons/home-screen-icons';

export const Register = () => {
  const { user } = useContext(AuthContext);
  const navigation = useNavigation();
  const { data } = useQuery(REQUEST_ADMISSION_DATA);
  const { collection }: any = useCollection(`users/${user?.uid}/admissions`);
  const admissionProcessData: any = _.intersectionBy(
    collection,
    data?.mainCourseCollection.items,
    'admissionId'
  )[0];

  return (
    <Box
      width={'100%'}
      height={274}
      role={'primary100'}
      justifyContent={'space-evenly'}
    >
      <Spacing pl={4}>
        <Text
          type={'title1'}
          role={'primary500'}
          bold
          numberOfLines={3}
          width={160}
          fontFamily={'Montserrat'}
        >
          БҮРТГЭЛ ЯВАГДАЖ БАЙНА
        </Text>
      </Spacing>
      <Spacing pl={4}>
        <Button
          onPress={() =>
            navigation.navigate(
              user === null
                ? NavigationRoutes.UserUndefined
                : admissionProcessData
                ? NavigationRoutes.Admission
                : NavigationRoutes.MainCourseRegistration
            )
          }
          width={183}
        >
          <Box flexDirection={'row'} alignItems={'center'}>
            <Spacing mr={1.5}>
              <Text fontFamily={'Montserrat'} role={'white'} bold>
                БҮРТГҮҮЛЭХ
              </Text>
            </Spacing>
            <View
              style={{
                flexDirection: 'column',
                transform: [{ rotate: '-90deg' }],
              }}
            >
              <ArrowIcon color={'white'} height={6} width={16} />
              <ArrowIcon color={'white'} height={6} width={16} />
            </View>
          </Box>
        </Button>
      </Spacing>
      <Box
        position={'absolute'}
        right={0}
        bottom={0}
        width={'auto'}
        height={'auto'}
      >
        <ComputerIcon />
      </Box>
    </Box>
  );
};

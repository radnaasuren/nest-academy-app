import React, { useState } from 'react';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { Svg, Path } from 'react-native-svg';
import { Spacing, Box, Border, Text, Stack, Queue } from '../../components';
import { Dot } from '../../components/icons/dot';

type filter = {
  level?: string;
  lessons?: any;
  duration?: string;
  number?: string;
};

export const CurriculmCard: React.FC<filter> = ({
  level,
  lessons,
  duration,
  number,
}) => {
  return (
    <Spacing mt={4}>
      <Border radius={4}>
        <Box role={'white'} width={343} height={'auto'}>
          <Spacing p={4}>
            <Box>
              <Text
                role={'primary500'}
                type={'body'}
                fontFamily={'Montserrat'}
                bold
              >
                {`${level}-р шат`}
              </Text>
            </Box>
          </Spacing>
          <Border lineWidth={1} role={'primary200'}></Border>
          <Spacing mt={4} />
          <Box height={'auto'} justifyContent={'space-around'}>
            <FlatList
              data={lessons}
              renderItem={({ item }: any) => {
                return <Queue>
                    <Spacing p={3}>
                      <Dot />
                    </Spacing>
                    <Text>{item}</Text>
                  </Queue>

              }}
              keyExtractor={(item) => item}
              ItemSeparatorComponent={() => <Box height={12} />}
            />
          </Box>
          <Spacing mt={2} />
          <Border lineWidth={1} role={'primary200'}></Border>

          <Spacing p={3}>
            <Box
              flexDirection={'row'}
              alignItems={'center'}
              width={'85%'}
              justifyContent={'space-between'}
              alignSelf={'center'}
            >
              <Box flexDirection={'row'}>
                <Spacing ml={2}>
                  <Text type={'subheading'} bold>
                    {duration}
                  </Text>
                </Spacing>
              </Box>
              <Box flexDirection={'row'}>
                <Box>
                  <Svg width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <Path
                      d="M18 16H3C2.73478 16 2.48043 16.1054 2.29289 16.2929C2.10536 16.4804 2 16.7348 2 17C2 17.2652 2.10536 17.5196 2.29289 17.7071C2.48043 17.8946 2.73478 18 3 18H18V20H3C2.20435 20 1.44129 19.6839 0.87868 19.1213C0.316071 18.5587 0 17.7956 0 17V2C0 1.46957 0.210714 0.960859 0.585786 0.585786C0.960859 0.210714 1.46957 0 2 0H18V16ZM2 14.05C2.162 14.017 2.329 14 2.5 14H16V2H2V14.05ZM13 7H5V5H13V7Z"
                      fill="#172B4D"
                    />
                  </Svg>
                </Box>
                <Spacing ml={2}>
                  <Text type={'subheading'} bold>
                    {number}
                  </Text>
                </Spacing>
              </Box>
            </Box>
          </Spacing>
        </Box>
      </Border>
    </Spacing>
  );
};

import React, { createContext, useRef, useContext, useState } from 'react';
import { FlatList, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { PhotoCard } from './photo-card';
import {
  Text,
  Spacing,
  Box,
  Stack,
  Queue,
  Border,
  Button,
  ExpandableText,
  TopBarNavigator,
  TopBarScreen,
  LoadingCircle,
  BackgroundImage,
  Toggle,
  TimeSchedule,
} from '../../components';
import { DangerIcon, PrimaryRightArrowIcon } from '../../components/icons'
import { AdmissionProcess } from './admission-process';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useQuery } from '@apollo/client';
import { REQUEST_MAIN_DETAIL_SCREEN } from '../../graphql/queries';
import _ from 'lodash';
import { AdvantagesCard } from './advantages-card';
import { CurriculmCard } from './curriculum-card';
import { ProjectScreen } from './project-screen';

const MainCourseContext = createContext({
  data: [],
  loading: true,
  courseLogo: '',
  advantages: [],
  courseDescription: '',
  timeTables: [],
  teachersInformation: [],
  admissionProcess: [],
  curriculumData: [],
  demoProjects: [],
});

export const MainCourse: React.FC<any> = ({ route }) => {
  const navigation = useNavigation();
  const { loading, courseLogo } = useContext(MainCourseContext);

  if (loading) {
    return (
      <Box flex={1} alignItems={'center'} justifyContent={'center'}>
        <LoadingCircle />
      </Box>
    );
  }

  const MainCoursePoster = () => {
    return (
      <BackgroundImage resizeMode={'cover'} width={'100%'} height={280} source={{ uri: courseLogo }} />
    );
  };

  return (
    <Box>
      <TopBarNavigator Header={MainCoursePoster}>
        <TopBarScreen name="Тухай" component={MainCourseAboutScreen} />
        <TopBarScreen name="Хөтөлбөр" component={HopProgramScreen} />
        <TopBarScreen name="Бүтээлүүд" component={ProjectScreen} />
      </TopBarNavigator>
      <Box bottom={0} position={'absolute'} width={'100%'} height={90} role={'white'}>
        <Spacing pt={1} pl={4} pr={4}>
          <Button
            size={'l'}
            width={'100%'}
            onPress={() => navigation.navigate(NavigationRoutes.Admission)}
          >
            <Text fontFamily={'Montserrat'} type={'callout'} role={'white'} bold>
              БҮРТГҮҮЛЭХ
          </Text>
          </Button>
        </Spacing>
      </Box>
    </Box>
  );
};


export const MainCourseAboutScreen = () => {
  const navigation = useNavigation();
  const { data, loading, advantages, courseDescription, timeTables, teachersInformation } = useContext(MainCourseContext);
  const teachers = teachersInformation.map((teacher: any, index: Number) => ({ ...teacher, index: index }));

  const [index, setIndex] = useState(0);

  const onViewRef = useRef((viewableItems: any) => {

    if (viewableItems.viewableItems.length > 0) {
      let currentIndex = viewableItems.viewableItems[0].index;
      setIndex(currentIndex);
    }

  });
  const viewConfigRef = useRef({
    viewAreaCoveragePercentThreshold: 50,
  });


  if (loading) {
    return (
      <Box flex={1} alignItems={'center'} justifyContent={'center'}>
        <LoadingCircle />
      </Box>
    );
  }

  return (
    <Box flex={1}>
      <Spacing ph={4} pt={9} pb={5}>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Давуу талууд
          </Text>
          <Spacing mt={8}>
            <Stack size={6}>
              <FlatList
                data={advantages}
                renderItem={({ item }: any) => {
                  console.log(Object.keys(advantages[0]))
                  return <AdvantagesCard
                    icon={item.icon.url}
                    title={item.title}
                    description={item.description}
                  />
                }}
                keyExtractor={({index}) => index}
                ItemSeparatorComponent={() => <Box height={24} />}
              />
            </Stack>
          </Spacing>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <ExpandableText text={courseDescription} />
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box width={'100%'}>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Цаг хувиарлалт
          </Text>
          <Spacing mt={4}>
            <Box height={'auto'} width={'100%'}>
              <Toggle sessions={timeTables.map((cur: any) => cur.session)}>
                {
                  _.map(timeTables, (item: any, index) => <Stack width={'100%'} key={index + 'nani'} size={6}>
                    <Border radius={8}>
                      <Box width={'100%'} role={'caution300'}>
                        <Spacing p={3}>
                          <Text role={'black'}>{item.description.json.content[0].content[0].value}</Text>
                        </Spacing>
                      </Box>
                    </Border>
                    <Stack size={2}>
                      {_.map(item.timeBlocks, ({ title, body, startDate, endData }) => <TimeSchedule title={title} from={endData} to={startDate}>{body}</TimeSchedule>)}
                    </Stack>
                  </Stack>
                  )
                }
              </Toggle>
            </Box>
          </Spacing>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <FlatList
            data={teachers}
            viewabilityConfig={viewConfigRef.current}
            onViewableItemsChanged={onViewRef.current}
            pagingEnabled={true}
            style={{ overflow: 'visible' }}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item, index }) => {
              return (
                <PhotoCard
                  key={item.index}
                  ratio={0.9}
                  source={{ uri: item.image.url }}
                  title={item.class}
                  name={item.name}
                  width={Dimensions.get('window').width - 40}
                />
              );
            }}
            keyExtractor={({ index }) => index}
            ItemSeparatorComponent={() => <Box width={20} />}
            horizontal
          />
          <Spacing mt={6} />
          <Text bold type={'body'}>
            Дэлгэрэнгүй
          </Text>
          <Spacing mt={2} />
          <Text numberOfLines={4}>
            {teachers[index].introduction}
          </Text>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Элсэлтийн явц
          </Text>
          <Spacing m={4} />
          <AdmissionProcess />
        </Box>
      </Spacing>
      <Spacing>
        <Border topWidth={1} bottomWidth={1} role="primary300">
          <Box height={56} justifyContent="center">
            <Spacing mh={5}>
              <Queue justifyContent="space-between">
                <Queue>
                  <Spacing>
                    <Text fontFamily={'Montserrat'} type="body" role="black">
                      FAQ
                    </Text>
                  </Spacing>
                </Queue>
                <Box position="absolute" right={5} bottom={-16}>
                  <Button
                    category="text"
                    status="active"
                    height={64}
                    onPress={() =>
                      navigation.navigate(NavigationRoutes.FaqScreen)
                    }
                  >
                     <PrimaryRightArrowIcon width={24} height={24} />
                  </Button>
                </Box>
              </Queue>
            </Spacing>
          </Box>
        </Border>
      </Spacing>
      <Spacing m={15} />
    </Box>
  );
};

export const HopProgramScreen = () => {
  const { curriculumData } = useContext(MainCourseContext);

  return (
    <Spacing p={4}>
      <Box width={'100%'} flex={1} alignItems={'center'}>
        <Spacing mt={4}>
          <Box height={'auto'} width={'100%'}>
            <Toggle sessions={curriculumData.map((cur: any) => cur.name)}>
              {
                curriculumData.map((cur: any) => <FlatList
                  data={cur.curriculum}
                  renderItem={(data: any) => {
                    return <CurriculmCard
                      level={data.item.level}
                      lessons={data.item.topics}
                      duration={'8 сар'}
                      number={'43 хичээл'}
                    />
                  }}
                  keyExtractor={(item) => item}
                  ItemSeparatorComponent={() => <Box width={16} />}
                />)
              }
            </Toggle>
          </Box>
        </Spacing>

      </Box>
    </Spacing>
  );
};


export const MainCourseScreen: React.FC<any> = ({ route }) => {
  const { entryId } = route.params || {};
  const { data, error, loading } = useQuery(REQUEST_MAIN_DETAIL_SCREEN, {
    variables: { entryId },
  });

  const courseLogo = _(data).get('mainCourse.courseLogo.url');
  const advantages = _(data).get('mainCourse.advantagesCollection.items');
  const courseDescriptionContent = _(data).get('mainCourse.courseDescription.json.content');
  const courseDescription = _.isArray(courseDescriptionContent) ? _(courseDescriptionContent).map((item: any) => _.get(_.head(item.content), 'value')).join('\n') : '';
  const teachersInformation = _(data).get('mainCourse.staffCollection.items');
  const timeTables = _(data).get('mainCourse.timetableCollection.items');
  const admissionProcess = _(data).get('mainCourse.admissionProcess');
  const curriculumData = _(data).get('mainCourse.curriculumCollection.items');
  const demoProjects = _(data).get('mainCourse.demoProjectsCollection.items');

  return <MainCourseContext.Provider
    value={{
      demoProjects,
      curriculumData,
      admissionProcess,
      teachersInformation,
      courseLogo,
      advantages,
      courseDescription,
      timeTables,
      data,
      loading
    }}>
    <MainCourse />
  </MainCourseContext.Provider>
}

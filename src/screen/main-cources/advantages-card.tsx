import React from 'react';
import { Image } from 'react-native';
import {
  BackgroundImage,
  Border,
  Box,
  Queue,
  Spacing,
  Stack,
  Text,
} from '../../components';

interface Texts {
  icon?: any;
  title?: string;
  description?: string;
}

export const AdvantagesCard: React.FC<Texts> = ({
  title,
  icon,
  description,
}) => {
  return (
    <Queue size={5.25}>
      <Box justifyContent={'center'} alignItems={'center'}>
        <BackgroundImage
          resizeMode={'stretch'}
          width={64}
          height={64}
          source={{ uri: icon }}
        />
      </Box>
      <Stack width={263} size={2}>
        <Text type={'body'} role={'primary500'} bold>
          {title}
        </Text>
        <Text type={'body'} width={263} numberOfLines={3} role={'primary500'}>
          {description}
        </Text>
      </Stack>
    </Queue>
  );
};

import React, { useState } from 'react';
import { FlatList, SafeAreaView, ScrollView, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';
import {
  BackgroundImage,
  Box,
  Spacing,
  Text,
  Border,
  Comment,
  Button,
  ArrowIcon
} from '../../components';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { REQUEST_DemoProject } from '../../graphql/queries';
import { useQuery } from '@apollo/client';

const CommentData = [
  {
    id: 1,
    source: require('../../assets/images/zedude.png'),
    name: 'Н. Цэрэнлхагва',
    title: '3-р түвшний сурагч',
    description:
      'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
  },
  {
    id: 2,
    source: require('../../assets/images/zedude.png'),
    name: 'Н. Цэрэнлхагва',
    title: '3-р түвшний сурагч',
    description:
      'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
  },
];

export const ProjectScreen = () => {
  const { data } = useQuery(REQUEST_DemoProject);
  const [height, setHeight] = useState(190)
  const navigation = useNavigation();
  const { width } = Dimensions.get('window');

  return (
    <SafeAreaView>
      <ScrollView>
        <Spacing mb={3} mt={5} ml={3}>
          <Text type="headline" role="primary500" bold textAlign="left">
            Demo Day 2020
          </Text>
        </Spacing>
        <Spacing mh={2}>
          <Box width={width - 16} justifyContent='flex-start' flexDirection='row' flexWrap='wrap' height={height} overflow='hidden'>
            {data && data.demoProjectsCollection.items.map((d: any) => {
              return (
                <Spacing m={1}>
                  <Border radius={4}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate(
                          NavigationRoutes.DemoDetails,
                          d
                        )
                      }
                    >
                      <BackgroundImage source={d.posterImage} width={width / 2 - 16} height={width / 2 - 16} />
                    </TouchableOpacity>
                  </Border>
                </Spacing>
              )
            })}
          </Box>
        </Spacing>
        <Spacing mh={4} mt={4}>
          <Button onPress={() => setHeight((Math.floor(data.demoProjectsCollection.items.length / 2) + 1) * 190)} category='text'>
            <Border lineWidth={1} role="primary500" radius={4}>
              <Box width={width - 32} height='100%' justifyContent='center' alignItems='center' flexDirection='row'>
                <Text textAlign="center" bold type="callout">
                  Өөр
                    </Text>
                <Spacing ml={3}>
                  <ArrowIcon />
                </Spacing>
              </Box>
            </Border>
          </Button>
        </Spacing>
        <Spacing mt={8} mh={4}>
          <Text type="title3" role="primary500" bold>
            Сурагцын сэтгэгдэл
          </Text>
        </Spacing>
        <Spacing mh={4} mt={6}>
          <FlatList
            data={CommentData}
            horizontal
            keyExtractor={(_, index) => index + 'comment'}
            renderItem={({ item }) => (
              <Comment
                name={item.name}
                source={item.source}
                title={item.title}
                description={item.description}
                width={311}
                height={288}
              />
            )}
          />
        </Spacing>
      </ScrollView>
    </SafeAreaView>
  );
};

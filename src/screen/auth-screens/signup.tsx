import React, { useContext, useState } from 'react';
import { ProgressSteps, ProgressStep, Box } from '../../components';
import { AuthContext } from '../../provider/auth';
import { Auth1, Auth2, Auth3 } from './auths/index';
import { useCollection } from '../../hooks';
import { useNavigation } from '@react-navigation/core';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const Signup = () => {
  const navigation = useNavigation();
  const [error, setError] = useState('passed');
  const { user, signInWithPhoneNumber, confirmCode } = useContext(AuthContext);
  const { updateRecord: createUser } = useCollection('users');
  const [state, setState] = useState({ firstName: '', lastName: '' });
  const section1 = async () => {
    await signInWithPhoneNumber();
  };
  const section2 = async () => {
    await confirmCode();
  };
  const section4 = async () => {
    await createUser(user && user.uid, state);
    await navigation.navigate(NavigationRoutes.Success, {
      successMessage:
        'Таны бүртгэл амжилттай үүслээ. Та манай аппликэшнд тавтай морил.',
    });
  };

  return (
    <Box flex={1} role={'white'}>
      <ProgressSteps lastButtonFunction={section4}>
        <ProgressStep label="Auth1" id="Auth1" onNext={() => section1()} >
          <Auth1 title=" Гар утасны дугаараа оруулна уу" />
        </ProgressStep>
        <ProgressStep
          label="Auth2"
          id="Auth2"
          onNext={section2}
          catchFunction={(e) => setError(e.code)}
        >
          <Auth2 error={error} />
        </ProgressStep>
        <ProgressStep label="Auth3" id="Auth3">
          <Auth3 setState={setState} />
        </ProgressStep>
      </ProgressSteps>
    </Box>
  );
};

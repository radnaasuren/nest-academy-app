import React from 'react';
import { SafeAreaView } from 'react-native';
import { Box, Text, Input, Spacing } from '../../../components';

export const Auth3 = ({ setState }: any) => {
  return (
    <Box flex={1} role={'white'}>
      <Spacing mt={5} mb={10}>
        <Box height={44} justifyContent="center" alignItems="center">
          <Text height={44} bold textAlign="center" width={258} type="headline">
            Та өөрийн мэдээллээ үнэн зөв оруулна уу
          </Text>
        </Box>
      </Spacing>
      <Spacing mt={12} mb={22}>
        <Box height={56} justifyContent="center" alignItems="center">
          <Box height={128} alignItems="center" justifyContent="center">
            <Input
              placeholder={'Овог Нэр'}
              type={'default'}
              onChangeText={(text: string) =>
                setState((state: any) => ({ ...state, lastName: text }))
              }
              width={'95%'}
            />
            <Spacing mt={4}>
              <Input
                placeholder={'Нэр'}
                type={'default'}
                onChangeText={(text: string) =>
                  setState((state: any) => ({ ...state, firstName: text }))
                }
                width={'95%'}
              />
            </Spacing>
          </Box>
        </Box>
      </Spacing>
    </Box>
  );
};

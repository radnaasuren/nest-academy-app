import React, { useContext } from 'react';
import { SafeAreaView } from 'react-native';
import { Box, Input, Text, Spacing } from '../../../components';
import { AuthContext } from '../../../provider/auth';

export const Auth1 = ({ title }: any) => {
  const { setnumber } = useContext(AuthContext);
  return (
    <Box flex={1} role={'white'}>
      <Spacing mt={5}>
        <Box height={44} justifyContent="center" alignItems="center">
          <Text height={44} width={200} bold textAlign="center" type="headline">
            {title}
          </Text>
        </Box>
      </Spacing>
      <Spacing mt={12} mb={22}>
        <Box height={56} justifyContent="center" alignItems="center">
          <Input
            width={'95%'}
            placeholder={'Утасны дугаар'}
            type={'default'}
            keyboardType={'numeric'}
            onChangeText={(text: string) => setnumber(text)}
          />
        </Box>
      </Spacing>
    </Box>
  );
};

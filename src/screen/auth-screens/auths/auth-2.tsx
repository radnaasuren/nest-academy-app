import React, { useState, useContext, useEffect } from 'react';
import { Box, Text, DigitInput, Spacing, CountdownTimer, Stack, Button } from '../../../components';
import { AuthContext } from '../../../provider/auth';

export const Auth2 = ({ error, seterror }: any) => {
  const [resend, setResend] = useState(false);
  const { resendCode, setcode, code } = useContext(AuthContext);
  return (
    <Box flex={1} role={'white'}>
      <Spacing mt={5} mh={4} grow={1}>
        <Box flex={1} justifyContent={'space-between'}>
          <Stack size={12} alignItems={'center'}>
            <Text
              height={44}
              bold
              textAlign="center"
              width={240}
              type="headline"
            >
              Таны дугаарт илгээсэн 6 оронтой кодыг оруулна уу
            </Text>
            <Box height={56} justifyContent="center" alignItems="center">
              <DigitInput />
            </Box>
          </Stack>
          {
            error ?
              <Spacing mb={4}>
                <Box width={'100%'} flexDirection={'row'} justifyContent={'center'}>
                  <Button onPress={() => { resendCode(); setResend(false); seterror(null), setcode('') }} category='text'>
                    <Text type={'subheading'} role={'destructive500'} underline>
                      Алдаа дахин шинэ код оруулна уу
                </Text>
                  </Button>
                </Box>
              </Spacing>
              :
              resend ?
                <Spacing mb={4}>
                  <Box width={'100%'} flexDirection={'row'} justifyContent={'center'}>
                    <Button onPress={() => { resendCode(); setResend(false) }} category='text'>
                      <Text type={'subheading'} role={'destructive500'} underline>
                        Дахин код авах
                      </Text>
                    </Button>
                  </Box>
                </Spacing>
                :
                <Spacing mb={8}>
                  <Box width={'100%'} flexDirection={'row'} justifyContent={'center'}>
                    <Text type={'subheading'} role={'darkgray'}>
                      Дахин код авах
                  </Text>
                    <CountdownTimer type={'subheading'} role={'darkgray'} from={30} onFinish={() => setResend(true)}></CountdownTimer>
                  </Box>
                </Spacing>
          }
        </Box>
      </Spacing>
    </Box>
  );
};

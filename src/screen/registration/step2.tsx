import React, { useContext } from 'react';
import {
  Box,
  Spacing,
  Text,
  Stack,
  ProgressStepsContext,
  InputMessage,
  GroupCheckBox,
  CheckBoxItem,
  BackgroundImage,
  Queue,
} from '../../components';
import { SignUpOthers, SignUpUser } from '../../components/icons';

const user = require('../../assets/images/user.png');
const users = require('../../assets/images/users.png');

interface Props {
  check?: Object;
}

export const Step2: React.FC<Props> = ({ check }: any) => {
  const { userInfo, setUserInfo } = useContext(ProgressStepsContext);

  const press = (text: string) => {
    setUserInfo((userInfo: any) => ({ ...userInfo, forWho: text }));
  };

  const unPress = () => {
    setUserInfo((userInfo: any) => ({ ...userInfo, forWho: '' }));
  };

  return (
    <Box flex={1} justifyContent="center" alignItems="center">
      <Spacing mt={4} mb={8}>
        <Text type={'headline'} bold textAlign={'center'} width={250}>
          Та хэнд зориулж бүртгэл үүсгэж байна вэ ?
        </Text>
      </Spacing>
      <Box flex={1}>
        <Stack size={4}>
          <GroupCheckBox horizontal>
            <CheckBoxItem
              onUnPress={() => unPress()}
              onPress={() => press('forMe')}
              checkbox
              index={0}
            >
              <Box width={'85%'}>
                <Spacing pl={4} pv={5}>
                  <Box flexDirection="row">
                    <Queue size={5}>
                      <Spacing>
                        <SignUpUser />
                      </Spacing>
                      <Spacing pt={1}>
                        <Box
                          height={50}
                          justifyContent="space-between"
                          flexDirection="column"
                        >
                          <Text bold type="headline">
                            Өөртөө
                          </Text>
                          <Text type="body" role={'gray'}>
                            Зөвхөн өөртөө
                          </Text>
                        </Box>
                      </Spacing>
                    </Queue>
                  </Box>
                </Spacing>
              </Box>
            </CheckBoxItem>
            <CheckBoxItem
              onUnPress={() => unPress()}
              onPress={() => press('forAnother')}
              checkbox
              index={1}
            >
              <Box width={'85%'}>
                <Spacing pl={4} pv={5}>
                  <Box flexDirection="row">
                    <Queue size={5}>
                      <Spacing>
                        <SignUpOthers />
                      </Spacing>
                      <Spacing pt={1}>
                        <Box
                          height={50}
                          justifyContent="space-between"
                          flexDirection="column"
                        >
                          <Text bold type="headline">
                            Бусад
                          </Text>
                          <Text type="body" role={'gray'}>
                            Хүүхэд, дүү, Найз
                          </Text>
                        </Box>
                      </Spacing>
                    </Queue>
                  </Box>
                </Spacing>
              </Box>
            </CheckBoxItem>
          </GroupCheckBox>
          <InputMessage role={check.forWho && check.forWho.messageType}>
            <Text role={'primary500'} type={'subheading'}>
              {check.forWho && check.forWho.messageText}
            </Text>
          </InputMessage>
        </Stack>
      </Box>
    </Box>
  );
};

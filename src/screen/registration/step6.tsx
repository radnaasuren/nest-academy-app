import React, { useContext } from 'react';
import { ScrollView } from 'react-native';
import {
  Border,
  Box,
  InputWithMessage,
  ProgressStepsContext,
  Spacing,
  Stack,
  Text,
  DropDown,
} from '../../components';

interface Props {
  check?: Object;
}

const Kid = ({ userInfo, setUserInfo, check }: any) => {
  return (
    <Stack size={4}>
      <InputWithMessage
        width={'100%'}
        messageText={check.school && check.school.messageText}
        messageType={check.school && check.school.messageType}
        placeholder={'Сургууль'}
        value={userInfo.school}
        onChangeText={(text: string) =>
          setUserInfo((userInfo: any) => ({ ...userInfo, school: text }))
        }
      />
      <InputWithMessage
        width={'100%'}
        messageText={check.grade && check.grade.messageText}
        messageType={check.grade && check.grade.messageType}
        placeholder={'Анги'}
        value={userInfo.grade}
        onChangeText={(text: string) =>
          setUserInfo((userInfo: any) => ({ ...userInfo, grade: text }))
        }
      />
    </Stack>
  );
};
const Adult = ({ userInfo, setUserInfo, check }: any) => {
  return (
    <Box width={'100%'}>
      <Stack size={4}>
        <DropDown.Provider>
          <DropDown.Trigger title={'Боловсролын байдал'}></DropDown.Trigger>
          <DropDown.Content
            contents={['Дунд', 'Бүрэн Дунд', 'Баклавр', 'Магистр']}
            chosenOne={(text: string) =>
              setUserInfo((userInfo: any) => ({
                ...userInfo,
                educationStatus: text,
              }))
            }
          >
            <Text type={'body'} role={'primary500'}>
              Дунд
            </Text>
            <Text type={'body'} role={'primary500'}>
              Бүрэн Дунд
            </Text>
            <Text type={'body'} role={'primary500'}>
              Баклавр
            </Text>
            <Text type={'body'} role={'primary500'}>
              Магистр
            </Text>
          </DropDown.Content>
        </DropDown.Provider>
        <InputWithMessage
          width={'100%'}
          messageText={check.description && check.description.messageText}
          messageType={check.description && check.description.messageType}
          placeholder={'Эрхэлж буй ажил, сургуулийн товч танилцуулга'}
          value={userInfo.description}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({ ...userInfo, description: text }))
          }
        />
      </Stack>
    </Box>
  );
};
const Contact = ({ userInfo, setUserInfo, check, age }: any) => {
  const phoneFunction = (text: any) => {
    if (text.length !== 9)
      setUserInfo((userInfo: any) => ({ ...userInfo, phone: text }));
  };

  return (
    <Stack size={4}>
      <Text type={'headline'} bold textAlign={'left'}>
        Холбоо барих
      </Text>
      <InputWithMessage
        width={'100%'}
        messageText={check.email && check.email.messageText}
        messageType={check.email && check.email.messageType}
        placeholder={'И-мейл'}
        value={userInfo.email}
        onChangeText={(text: string) =>
          setUserInfo((userInfo: any) => ({ ...userInfo, email: text }))
        }
        keyboardType={'email-address'}
      />
      <InputWithMessage
        width={'100%'}
        messageText={check.phone && check.phone.messageText}
        messageType={check.phone && check.phone.messageType}
        placeholder={'Яаралтай үед холбоо барих'}
        value={userInfo.phone}
        onChangeText={(text: string) => phoneFunction(text)}
        keyboardType={'number-pad'}
      />
      {age < 18 ? (
        <></>
      ) : (
        <InputWithMessage
          width={'100%'}
          messageText={check.facebook && check.facebook.messageText}
          messageType={check.facebook && check.facebook.messageType}
          placeholder={'Таны Facebook хаяг'}
          value={userInfo.facebook}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({ ...userInfo, facebook: text }))
          }
        />
      )}
    </Stack>
  );
};
const FromWho = ({ userInfo, setUserInfo, check }: any) => {
  return (
    <Stack size={4}>
      <Text type={'headline'} bold textAlign={'left'} width={343}>
        Хаанаас олж мэдсэн бэ?
      </Text>
      <DropDown.Provider>
        <DropDown.Trigger title={'Facebook'}></DropDown.Trigger>
        <DropDown.Content
          contents={['Найз', 'Фэйсбүүк', 'Эцэг эх', 'Зар сурталчилгаа']}
          setChose={(text: string) =>
            setUserInfo((userInfo: any) => ({
              ...userInfo,
              fromWho: text,
            }))
          }
        >
          <Text type={'body'} role={'primary500'}>
            Найз
          </Text>
          <Text type={'body'} role={'primary500'}>
            Фэйсбүүк
          </Text>
          <Text type={'body'} role={'primary500'}>
            Эцэг эх
          </Text>
          <Text type={'body'} role={'primary500'}>
            Зар сурталчилгаа
          </Text>
        </DropDown.Content>
      </DropDown.Provider>
    </Stack>
  );
};

export const Step6: React.FC<Props> = ({ check }) => {
  const { userInfo, setUserInfo } = useContext(ProgressStepsContext);
  const currentYear = new Date().getFullYear();
  const age = currentYear - Number(userInfo.date.slice(0, 4));

  return (
    <Box flex={1}>
      <ScrollView
        contentContainerStyle={{ alignItems: 'center' }}
        showsVerticalScrollIndicator={false}
      >
        <Spacing mv={8} pl={5.55} pr={5.55}>
          <Stack size={6}>
            <Stack size={2} alignItems={'flex-start'}>
              <Text type={'headline'} bold textAlign={'left'}>
                Элсэгчийн мэдээлэл
              </Text>
              <Text
                width={300}
                role={'primary400'}
                type={'body'}
                textAlign={'left'}
              >
                Та бидэнд үнэн зөв мэдээлэл өгсөнөөр бүртгэл амжилттай дуусна
              </Text>
            </Stack>

            {age < 18 ? (
              <Kid
                check={check}
                userInfo={userInfo}
                setUserInfo={setUserInfo}
              />
            ) : (
              <Adult
                check={check}
                userInfo={userInfo}
                setUserInfo={setUserInfo}
              />
            )}
          </Stack>

          <Spacing mb={5} mt={5}>
            <Border radius={3} lineWidth={1} role={'offwhite'} />
          </Spacing>

          <Contact
            age={age}
            check={check}
            userInfo={userInfo}
            setUserInfo={setUserInfo}
          />

          <Spacing mv={5}>
            <Border radius={3} lineWidth={1} role={'offwhite'} />
          </Spacing>

          <FromWho
            check={check}
            userInfo={userInfo}
            setUserInfo={setUserInfo}
          />
        </Spacing>
      </ScrollView>
    </Box>
  );
};

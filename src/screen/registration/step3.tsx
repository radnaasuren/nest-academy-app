import React, { useContext } from 'react';
import {
  Box,
  Text,
  Spacing,
  ProgressStepsContext,
  InputWithMessage,
  Stack,
} from '../../components';

interface Props {
  check?: Object;
}

export const Step3: React.FC<Props> = ({ check }: any) => {
  const { userInfo, setUserInfo } = useContext(ProgressStepsContext);

  return (
    <Box flex={1} justifyContent="center" alignItems="center">
      <Spacing mt={12} mb={8} mh={4} grow={1}>
        <Stack width={'100%'} size={2} alignItems={'center'}>
          <Text type={'headline'} bold textAlign={'center'}>
            Та энэ нэрээр үргэлжлүүлэх үү?
          </Text>
          <Text role={'primary400'} type={'body'} textAlign={'center'}>
            Та бүртгүүлэхийг хүссэн хүнийхээ нэрийг бүтэн, зөв бичнэ үү
          </Text>
        </Stack>

        <Box flex={1}>
          <Spacing mt={8}>
            <InputWithMessage
              width={'100%'}
              placeholder={'Овог Нэр'}
              type={'default'}
              messageText={check.name && check.name.messageText}
              messageType={check.name && check.name.messageType}
              value={userInfo.name}
              onChangeText={(text: string) =>
                setUserInfo((userInfo: any) => ({ ...userInfo, name: text }))
              }
              onSubmitEditing={() => console.log('nani')}
            />
          </Spacing>
        </Box>
      </Spacing>
    </Box>
  );
};

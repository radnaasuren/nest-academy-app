import React, { useState, useContext, useEffect } from 'react';
import { useNavigation } from '@react-navigation/core';
import { Box, ProgressSteps, ProgressStep } from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { Step1 } from './step1';
import { Step2 } from './step2';
import { Step3 } from './step3';
import { Step4 } from './step4';
import { Step5 } from './step5';
import { Step6 } from './step6';
import { useDocument, useCollection } from '../../hooks/firebase';
import { AuthContext } from '../../provider/auth';
import { useQuery } from '@apollo/client';
import { REQUEST_ADMISSION_DATA } from '../../graphql';
import _ from 'lodash';

export const Registration = () => {
  const navigation = useNavigation();
  const { data } = useQuery(REQUEST_ADMISSION_DATA);
  const year = new Date().getFullYear();
  const [userInfo, setUserInfo] = useState<any>({});
  const [check, setCheck] = useState<any>({
    name: undefined,
    date: '2000/02/21',
    gender: undefined,
    grade: undefined,
    school: undefined,
    educationStatus: undefined,
    fromWho: undefined,
    phone: undefined,
    email: undefined,
    facebook: undefined,
    description: undefined,
  });
  const age = year - Number(userInfo.date?.split('/')[0]);
  const course = _(data).get(`mainCourseCollection.items`);
  const admissionData = _.find(course, {
    courseTitle: age < 18 ? 'Hop' : 'Leap',
  });

  let { user } = useContext(AuthContext);
  let { uid } = user || {};
  const { updateRecord } = useCollection(`users/${uid}/admissions`);
  const { doc: userData, updateRecord: updateProfile } = useDocument(
    `users/${uid}`
  );

  useEffect(() => {
    if (userData !== null) setUserInfo({ ...userInfo, userData });
  }, [userData]);

  const step2 = () => {
    if (userInfo) {
      setCheck((check: any) => ({
        ...check,
        forWho: {
          messageType: userInfo.forWho === '' ? 'error' : undefined,
          messageText:
            userInfo.forWho === '' ? 'Та энэ хэсгийг бөглөнө үү!!' : undefined,
        },
      }));
      if (userInfo.forWho === '') throw 'forWho field empty';
    }
  };

  const step3 = () => {
    if (userInfo) {
      setCheck((check: any) => ({
        ...check,
        name: {
          messageType: userInfo.name === '' ? 'error' : undefined,
          messageText:
            userInfo.name === '' ? 'Та энэ хэсгийг бөглөнө үү!!' : undefined,
        },
      }));

      if (userInfo.name === '') throw 'name field empty';
    }
  };
  const step4 = () => {
    if (userInfo) {
      setCheck((check: any) => ({
        ...check,
        date: {
          messageType: userInfo.date.length !== 10 ? 'error' : undefined,
          messageText:
            userInfo.date.length !== 10
              ? 'Та энэ хэсгийг бөглөнө үү!!'
              : undefined,
        },
        gender: {
          messageType: userInfo.gender === '' ? 'error' : undefined,
          messageText:
            userInfo.gender === '' ? 'Та энэ хэсгийг бөглөнө үү!!' : undefined,
        },
      }));

      if (userInfo.date.length !== 10 || userInfo.gender === '')
        throw 'date or gender field empty';
    }
  };
  const onSubmit = async () => {
    if (userInfo) {
      setCheck((check: any) => ({
        ...check,
        grade: {
          messageType: userInfo.grade === '' ? 'error' : undefined,
          messageText:
            userInfo.grade === '' ? 'Та энэ хэсгийг бөглөнө үү!!' : undefined,
        },
        school: {
          messageType: userInfo.school === '' ? 'error' : undefined,
          messageText:
            userInfo.school === '' ? 'Та энэ хэсгийг бөглөнө үү!!' : undefined,
        },
        educationStatus: {
          messageType: userInfo.educationStatus === '' ? 'error' : undefined,
          messageText:
            userInfo.educationStatus === ''
              ? 'Та энэ хэсгийг бөглөнө үү!!'
              : undefined,
        },
        description: {
          messageType: userInfo.description === '' ? 'error' : undefined,
          messageText:
            userInfo.description === ''
              ? 'Та энэ хэсгийг бөглөнө үү!!'
              : undefined,
        },
        facebook: {
          messageType: userInfo.facebook === '' ? 'error' : undefined,
          messageText:
            userInfo.facebook === ''
              ? 'Та энэ хэсгийг бөглөнө үү!!'
              : undefined,
        },
        email: {
          messageType: userInfo.email === '' ? 'error' : undefined,
          messageText:
            userInfo.email === '' ? 'Та энэ хэсгийг бөглөнө үү!!' : undefined,
        },
        phone: {
          messageType: userInfo.phone.length !== 8 ? 'error' : undefined,
          messageText:
            userInfo.phone.length !== 8
              ? 'Та энэ хэсгийг бөглөнө үү!!'
              : undefined,
        },
        fromWho: {
          messageType: userInfo.fromWho === '' ? 'error' : undefined,
          messageText:
            userInfo.fromWho === '' ? 'Та энэ хэсгийг бөглөнө үү!!' : undefined,
        },
      }));
      if (userInfo.course === 'hop') {
        if (userInfo.grade === '') throw 'grade field empty';
        console.log('this will not gonna shown');
        if (userInfo.school === '') throw 'school field empty';
      } else {
        if (userInfo.educationStatus === '')
          throw 'educationStatus field empty';
        if (userInfo.description === '') throw 'description field empty';
        if (userInfo.facebook === '') throw 'facebook field empty';
      }
      if (userInfo.email === '') throw 'email field empty';
      if (userInfo.phone === '') throw 'phone field empty';
      if (userInfo.fromWho === '') throw 'fromWho field empty';
      console.log(userInfo)

      updateRecord(admissionData.admissionId, {
        ...userInfo,
        admissionStep: 2,
        admissionId: admissionData.admissionId,
        age,
      });
      navigation.navigate(NavigationRoutes.RegistrationSuccessScreen, {
        admissionId: admissionData.admissionId,
      });
    }
  };

  return (
    <Box flex={1} role={'white'}>
      <ProgressSteps
        lastButtonFunction={() => onSubmit()}
        setState={setUserInfo}
      >
        <ProgressStep label={'screen1'} id={'screen1'}>
          <Step1 />
        </ProgressStep>
        <ProgressStep label={'screen2'} id={'screen2'} onNext={step2}>
          <Step2 check={check} />
        </ProgressStep>
        <ProgressStep label={'screen3'} id={'screen3'} onNext={step3}>
          <Step3 check={check} />
        </ProgressStep>
        <ProgressStep label={'screen4'} id={'screen4'} onNext={step4}>
          <Step4 check={check} />
        </ProgressStep>
        <ProgressStep label={'screen5'} id={'screen5'}>
          <Step5 />
        </ProgressStep>
        <ProgressStep label={'screen6'} id={'screen6'}>
          <Step6 check={check} />
        </ProgressStep>
      </ProgressSteps>
    </Box>
  );
};

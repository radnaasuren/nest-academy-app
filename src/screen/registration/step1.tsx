import React from 'react';
import { Text, Spacing, Box, Chilling } from '../../components';

export const Step1 = () => {
  return (
    <Box flex={1} justifyContent="center" alignItems="center">
      <Box flex={1}>
        <Spacing ph={15} pv={10} grow={1}>
          <Box flex={1} justifyContent="center" alignItems="center">
            <Chilling />
            <Spacing mt={8}>
              <Text type={'title2'} bold textAlign={'center'}>
                Бүртгүүлэх
              </Text>
            </Spacing>
            <Spacing mt={6} mb={9}>
              <Text
                type={'body'}
                textAlign={'center'}
                role={'primary400'}
                width={250}
              >
                Та хөтөлбөрт бүргүүлэхэд ердөө 2 минут шаардагдах бөгөөд та
                мэдээллээ үнэн зөв бөглөнө үү
              </Text>
            </Spacing>
          </Box>
        </Spacing>
      </Box>
    </Box>
  );
};

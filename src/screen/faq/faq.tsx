import React, { useState } from 'react';
import { FlatList } from 'react-native';
import { Collapse, Spacing, Box } from '../../components';
import { useQuery } from '@apollo/client';
import { REQUEST_FAQ_SCREEN } from '../../graphql';

export const FaqScreen = () => {
  const [indexOfOpenedQuestion, setIndexOfOpenedQuestion] = useState(null);
  const { data } = useQuery(REQUEST_FAQ_SCREEN);
  const faq = data?.farCollection?.items;
  return (
    <Box flex={1} role={'white'}>
      {data && (
        <Spacing p={4}>
          <FlatList
            data={faq}
            renderItem={({ item, index }: any) => (
              <Collapse
                index={index}
                body={item.textAnswer.json.content[0].content[0].value}
                title={item.question}
                url={item.videoAnswer.url && item.videoAnswer.url}
                indexOfOpenedQuestion={indexOfOpenedQuestion}
                setIndexOfOpenedQuestion={setIndexOfOpenedQuestion}
              />
            )}
            keyExtractor={(item, index) => item + index}
            showsHorizontalScrollIndicator={false}
            ItemSeparatorComponent={() => <Spacing mt={3} />}
          />
        </Spacing>
      )}
    </Box>
  );
};

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useContext } from 'react';
import {
  HomeScreen,
  TestScreen,
  ExamWarningScreen1,
  ExamWarningScreen2,
  ExamWarningScreen3,
  ExamWarningScreen4,
  ExamScreen,
  CourseScreen,
  AdmissionScreen,
  PersonalInformationScreen,
  Registration,
  NotificationScreen,
  ScheduleMeetingCalendarScreen,
  ScheduleMeetingTimeScreen,
  ExamResultScreen,
} from '../screen';
import MainBottomNavigation from './main-bottom-navigation';
import {
  SplashScreen,
  MicroCoursePopupScreen,
  RegistrationSuccessScreen,
  WelcomeNestScreen,
} from '../screen';
import {
  NoInternetConnectionScreen,
  SomethingWentWrongScreen,
  EnableNotificationScreen,
  UserUndefined,
} from '../screen/modal-screens';
import { MainCourseScreen, DemoDetailsScreen } from '../screen/main-cources';
import { NavigationRoutes, NavigatorParamList } from './navigation-params';
import { SuccessScreen } from '../screen/success';
import { Signup } from '../screen/auth-screens/signup';
import { Login } from '../screen/auth-screens/login';
import { FaqScreen } from '../screen/faq';
import { FreeCourseScreen } from '../screen/free-course/free-course-screen';
import { News } from '../screen/news/news';
import { HeaderLeft, HeaderRight, HeaderTitle } from './headers';
import { Box, CountdownTimer, Spacing, Text } from '../components';
const RootStack = createStackNavigator<NavigatorParamList>();

export const ExamStack = () => {
  return (
    <RootStack.Navigator
      initialRouteName={NavigationRoutes.ExamWarning1}
      screenOptions={{
        gestureEnabled: false,
        headerLeft: (props) => <HeaderLeft {...props} />,
      }}
    >
      <RootStack.Screen
        name={NavigationRoutes.ExamWarning1}
        component={ExamWarningScreen1}
        options={({ navigation }) => ({
          headerLeft: () => <></>,
          headerTitle: () => <HeaderTitle title={'Шалгалт'} />,
          headerRight: () => (
            <HeaderRight
              onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
            />
          ),
        })}
      />
      <RootStack.Screen
        name={NavigationRoutes.ExamWarning2}
        component={ExamWarningScreen2}
        options={({ navigation }) => ({
          headerLeft: () => <></>,
          headerTitle: () => <HeaderTitle title={'Шалгалт'} />,
          headerRight: () => (
            <HeaderRight
              onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
            />
          ),
        })}
      />
      <RootStack.Screen
        name={NavigationRoutes.ExamWarning3}
        component={ExamWarningScreen3}
        options={({ navigation }) => ({
          headerLeft: () => <></>,
          headerTitle: () => <HeaderTitle title={'Шалгалт'} />,
          headerRight: () => (
            <HeaderRight
              onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
            />
          ),
        })}
      />
      <RootStack.Screen
        name={NavigationRoutes.ExamWarning4}
        component={ExamWarningScreen4}
        options={({ navigation }) => ({
          headerLeft: () => <></>,
          headerTitle: () => <HeaderTitle title={'Шалгалт'} />,
          headerRight: () => (
            <HeaderRight
              onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
            />
          ),
        })}
      />
      <RootStack.Screen
        name={NavigationRoutes.ExamScreen}
        component={ExamScreen}
        options={({ navigation }) => ({
          headerShown: false,
        })}
      />
    </RootStack.Navigator>
  );
};

export const CourseRegistrationStack = () => {
  return (
    <RootStack.Navigator
      initialRouteName={NavigationRoutes.ExamWarning1}
      screenOptions={{
        gestureEnabled: false,
        headerLeft: (props) => <HeaderLeft {...props} />,
      }}
    >
      <RootStack.Screen
        name={NavigationRoutes.Registration}
        component={Registration}
        options={({ navigation }) => ({
          headerLeft: () => <></>,
          headerTitle: () => <HeaderTitle title={'Бүртгэл'} />,
          headerRight: () => (
            <HeaderRight
              onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
            />
          ),
        })}
      />
      <RootStack.Screen
        name={NavigationRoutes.RegistrationSuccessScreen}
        component={RegistrationSuccessScreen}
        options={{
          headerShown: false,
        }}
      />
    </RootStack.Navigator>
  );
};

export const BookMeetingStack = () => {
  return (
    <RootStack.Navigator
      initialRouteName={NavigationRoutes.ScheduleMeetingCalendar}
      screenOptions={{
        gestureEnabled: false,
        headerLeft: (props) => <HeaderLeft {...props} />,
      }}
    >
      <RootStack.Screen
        name={NavigationRoutes.ScheduleMeetingCalendar}
        component={ScheduleMeetingCalendarScreen}
        options={{
          headerTitle: () => <HeaderTitle title={'Ярилцлага'} />,
        }}
      />
      <RootStack.Screen
        name={NavigationRoutes.ScheduleMeetingTime}
        component={ScheduleMeetingTimeScreen}
      />
    </RootStack.Navigator>
  );
};

export const RootNavigationContainer = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        initialRouteName={NavigationRoutes.Splash}
        screenOptions={{
          gestureEnabled: false,
          headerLeft: (props) => <HeaderLeft {...props} />,
        }}
      >
        <RootStack.Screen
          name={NavigationRoutes.Splash}
          component={SplashScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.MainRoot}
          component={MainBottomNavigation}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.News}
          component={News}
          options={{ headerTitle: () => <HeaderTitle title={'Мэдээ'} /> }}
        />
        <RootStack.Screen
          name={NavigationRoutes.MainCourceScreen}
          component={MainCourseScreen}
          options={{ headerTitle: () => <HeaderTitle title={'HOP'} /> }}
        />
        <RootStack.Screen
          name={NavigationRoutes.FaqScreen}
          component={FaqScreen}
          options={{ headerTitle: () => <HeaderTitle title={'АСУУЛТУУД'} /> }}
        />
        <RootStack.Screen
          name={NavigationRoutes.Course}
          component={CourseScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.Test}
          component={TestScreen}
          options={{ headerTitle: () => <HeaderTitle title={'Component'} /> }}
        />
        <RootStack.Screen
          name={NavigationRoutes.DemoDetails}
          component={DemoDetailsScreen}
        />
        <RootStack.Screen
          name={NavigationRoutes.FreeCourseScreen}
          component={FreeCourseScreen}
          options={{ headerTitle: () => <HeaderTitle title={'Суурь онол'} /> }}
        />
        <RootStack.Screen
          name={NavigationRoutes.ExamResultScreen}
          component={ExamResultScreen}
          options={{ headerShown: false }}
        />

        <RootStack.Screen
          name={NavigationRoutes.MicroCoursePopupScreen}
          component={MicroCoursePopupScreen}
          options={{
            cardStyle: { backgroundColor: 'transparent' },
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.NoInternetConnectionScreen}
          component={NoInternetConnectionScreen}
          options={{
            cardStyle: { backgroundColor: 'transparent' },
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.UserUndefined}
          component={UserUndefined}
          options={{
            cardStyle: { backgroundColor: 'transparent' },
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.SomethingWentWrongScreen}
          component={SomethingWentWrongScreen}
          options={{
            cardStyle: { backgroundColor: 'transparent' },
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.EnableNotificationScreen}
          component={EnableNotificationScreen}
          options={{
            cardStyle: { backgroundColor: 'transparent' },
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.SuccessScreen}
          component={SuccessScreen}
          options={({ navigation }) => ({
            headerLeft: () => <></>,
            headerTitle: () => <></>,
            headerRight: () => (
              <HeaderRight
                onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
              />
            ),
          })}
        />

        <RootStack.Screen
          name={NavigationRoutes.LogIn}
          component={Login}
          options={({ navigation }) => ({
            headerLeft: () => <></>,
            headerTitle: () => <HeaderTitle title={'Нэвтрэх'} />,
            headerRight: () => (
              <HeaderRight
                onPress={() => {
                  navigation.navigate(NavigationRoutes.MainRoot);
                }}
              />
            ),
          })}
        />
        <RootStack.Screen
          name={NavigationRoutes.SignUp}
          component={Signup}
          options={({ navigation }) => ({
            headerLeft: () => <></>,
            headerTitle: () => <HeaderTitle title={'Бүртгүүлэх'} />,
            headerRight: () => (
              <HeaderRight
                onPress={() => {
                  navigation.navigate(NavigationRoutes.MainRoot);
                }}
              />
            ),
          })}
        />
        {/* <RootStack.Screen
          name={NavigationRoutes.WelcomeScreen}
          component={WelcomeNestScreen}
          options={{ 
            headerTitle: () => <HeaderTitle title={'Та'} />,
          }}
        /> */}

        <RootStack.Screen
          name={NavigationRoutes.Success}
          component={SuccessScreen}
          options={{ headerShown: false }}
        />

        <RootStack.Screen
          name={NavigationRoutes.ExamStack}
          component={ExamStack}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.BookMeeting}
          component={BookMeetingStack}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.MainCourseRegistration}
          component={CourseRegistrationStack}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.Admission}
          component={AdmissionScreen}
          options={{
            headerTitle: () => <HeaderTitle title={'Элсэлтийн явц'} />,
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.PersonalInformation}
          component={PersonalInformationScreen}
          options={{
            headerTitle: () => <HeaderTitle title={'Хувийн мэдээлэл'} />,
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.NotificationScreen}
          component={NotificationScreen}
          options={{
            headerTitle: () => <HeaderTitle title={'Notification'} />,
          }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

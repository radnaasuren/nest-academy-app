import { gql } from "@apollo/client";

export const REQUEST_ABOUT = gql`
    query Abouts {
        aboutCollection {
            total
            items {
                heading1
            }
        }
    }
`;


export const REQUEST_NEWS = gql`
    query DemoProjects {
        blogPostCollection(where: {picture_exists: true}) {
            items{
                sys{
                    id
                },
                title,
                publishedDate,
                shortDescription,
                description{
                    json
                }
                picture {
                    title
                    description
                    contentType
                    fileName
                    size
                    url
                    width
                    height
                },
            }
        }
    }
`;

export const REQUEST_MAIN_DETAIL_SCREEN = gql`
query mainCourse($entryId: String!) {
  mainCourse(id: $entryId) {
      courseTitle
      courseLogo{url}
      courseDescription{json}
      advantagesCollection{
        items{
          sys {
            id
          }
          title
          description
          icon{url}
        }
      }
      staffCollection{
        items{
          sys {
            id
          }
          name
          position
          image{url}
          introduction
        }
      }
      timetableCollection{
        items{
          sys {
            id
          }
          timeBlocks
          description{json}
          session
        }
      }
      curriculumCollection{
        items{
          sys {
            id
          }
          curriculum
          name
        }
      }
      demoProjectsCollection (limit: 10){
        items{
          projectTitle
          posterImage{url}
          description
          introduction
          commentsCollection{
            items{
              sys{
                id
              }
              author {
                name
              }
              body{json}
            }
          }
          imagesCollection{
            items{
              url
            }
          }
        }
      }
        admissionProcess
    }
  }  
`

export const REQUEST_EXAM_SCREEN = gql`
query {
  exam(id: "2zPclPhu4abqZdjQ42Kauh") {
    mathCollection {
      total
      items {
        sys {
          id
        }
        question {
          json
        }
        type
        questionPicture {
          url
        }
        answerPicture {
          url
        }
        answer
        point
      }
    }
    designCollection {
      total
      items {
        sys {
          id
        }
        question {
          json
        }
        type
        questionPicture {
          url
        }
        answerPicture {
          url
        }
        answer
        point
      }
    }
    logicCollection {
      total
      items {
        sys {
          id
        }
        question {
          json
        }
        type
        questionPicture {
          url
        }
        answerPicture {
          url
        }
        answer
        point
      }
    }
    iqCollection {
      total
      items {
        sys {
          id
        }
        question {
          json
        }
        type
        questionPicture {
          url
        }
        answerPicture {
          url
        }
        answer
        point
      }
    }
  }
}
`
export const REQUEST_EXAM_SCREEN2 = gql`
query {
    exam(id: "2zPclPhu4abqZdjQ42Kauh") {
     title
    }
  }
`

export const Request_FAQ_ANSWER_VIDEOS = gql`
    query Faq_answer_videos {
        frequentlyAskedQuestionsFaqCollection {
            items {
                answerVideosCollection {
                    items {
                        url
                    }
                }
            }
        }
    }
`

export const REQUEST_FAQ_SCREEN = gql`
query {
    farCollection {
        items {
            question
            videoAnswer {
                url 
            }
            textAnswer {
                json    
            }
        }
    }
}
`
export const REQUEST_MICRO_COURSES = gql`
    query REQUEST_COURSES {
        microCourseCollection(limit: 20) {
            items {
              title
              sys {
                id
              }
              description {
                json
              }
              knowledge
              teachersCollection {
                items {
                  name
                  position
                  image {
                                url
                  }
                }
              }
                lessonsCollection {
                items {
                    name
                    article
                    isLock
                    sys {
                        id
                    }
                  video {
                    url
                  }
                  posterImage {
                    url
                  }
                }
              }
              posterImage {
                url        
              }
            }
        }
    }
`

export const REQUEST_LESSONS = gql`
    query REQUEST_LESSONS {
        lessonCollection {
            items {
                linkedFrom {
                    microCourseCollection {
                        items {
                            title
                        }
                    }
                }
              name
              article
              isLock
              sys {
                id
              }
              posterImage {
                url
              }
              video {
                url
              }
            }
        }
    }
`

export const REQUEST_MAIN_COURSES = gql`
    query REQUEST_MAIN_COURSES {
        mainCourseCollection {
            items {
                sys {
                    id
                }
                courseTitle
                courseLogo {
                    url
                }
            }
        }
    }
`

export const REQUEST_ADMISSION_DATA = gql`
    query REQUEST_ADMISSION_DATA {
      mainCourseCollection {
        items {
          courseTitle,
          admissionId,
          admissionProcess
        }
      }
    }
`
export const REQUEST_HOME_SCREEN = gql`
  query {
    newsCollection {
      items {
        sys {
          id
        }
        title
        category
        posterImage {
          url
        }
      }
    }
    mainCourseCollection {
      items {
        sys {
          id
        }
        courseTitle
        courseLogo {
          url
        }
      }
    }
  }
`
export const REQUEST_NEWS_CARD = gql`
query news($entryId: String!) {
  news(id: $entryId) {
      sys {
        publishedAt
      }
      title
      category
      posterImage {
        url
      }
      paragraph {
        json
      }
      assetsCollection {
        items {
          sys {
            id
          }
          url
        }
      }
      paragraph2 {
        json
      }
    }
}
`

export const REQUEST_DemoProject = gql`
query {
    demoProjectsCollection {
      items {
        sys {
          id
        }
        posterImage {
          url
        }
        description,
        imagesCollection {
          items {
            url
          }
        }
      }
    }
  }
`

export const Request_DemoProjects_Info = gql`
  query demoProjects($entryId: String!) {
      demoProjects(id: $entryId) {
        studentsCollection {
          items {
            name,
            image {
              url
            },
            class
          }
        },
        commentsCollection {
          items {
            author {
              name,
              position,
              image {
                url
              }
            },
            body {
              json
            }
          }
        }
      }
    }
`
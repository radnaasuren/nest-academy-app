import React from 'react';
import {
  Box,
  Text,
  Shadow,
  Button,
  Spacing,
  RightArrowIcon,
} from '../../components';
import { Border } from './border';
import { useNavigation } from '@react-navigation/native';
import { Dimensions } from 'react-native';
 
export const Warning: React.FC<WarningType> = ({
  title,
  buttonname,
  twobutton = true,
  children,
  onPress,
}) => {
  const navigation = useNavigation();
  const { width } = Dimensions.get('window');

  return (
    <Box height={210} width={'100%'}>
      <Shadow role="primary500" opacity={0.25} w={2}>
        <Shadow role="primary500" opacity={0.15} w={2}>
          <Border radius={4}>
            <Box height={189} width={'100%'} role="primary100">
              <Box flex={1} role="primary100" alignItems="center">
                <Spacing mt={6}>
                  <Text
                    role="primary500"
                    type="title3"
                    bold={true}
                    textAlign="center"
                  >
                    {title}
                  </Text>
                </Spacing>
                <Spacing mt={4}>{children}</Spacing>
              </Box>
            </Box>
          </Border>
        </Shadow>
      </Shadow>
      <Box width={'100%'} position="absolute" bottom={0}>
        {twobutton ? (
          <Box width="100%" flexDirection="row" justifyContent="space-between">
            <Button
              onPress={() => navigation.goBack()}
              width={167}
              category="ghost"
              size={'l'}
            >
              <Box
                justifyContent="center"
                alignItems="center"
                role="white"
                width={167}
                height="100%"
              >
                <Text fontFamily="Montserrat" type="callout" bold>
                  ӨМНӨХ
                </Text>
              </Box>
            </Button>
            <Button onPress={onPress} width={167} size={'l'}>
              <Box
                flexDirection="row"
                flex={1}
                justifyContent="center"
                alignItems="center"
              >
                <Text fontFamily="Montserrat" type="callout" role="white" bold>
                  {buttonname.toUpperCase()}
                </Text>
                <Box width={30} alignItems="center">
                  <RightArrowIcon />
                </Box>
              </Box>
            </Button>
          </Box>
        ) : (
          <Button onPress={onPress} category="fill" size="l" width={width - 60}>
            <Box
              flexDirection="row"
              justifyContent="center"
              alignItems="center"
            >
              <Box>
                <Text
                  type="callout"
                  role="white"
                  bold={true}
                  fontFamily="Montserrat"
                >
                  {buttonname.toUpperCase()}
                </Text>
              </Box>
              <Box width={40} justifyContent="center" alignItems="center">
                <RightArrowIcon />
              </Box>
            </Box>
          </Button>
        )}
      </Box>
    </Box>
  );
};

type WarningType = {
  title: string;
  buttonname: string;
  onPress: Function;
  twobutton?: boolean;
  children?: any;
  titleAlign?: 'center' | 'left';
};

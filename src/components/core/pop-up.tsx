import React from 'react';
import { StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Box, Queue, Spacing, Stack } from '../layout';
import { CloseLineIcon } from '../icons';
import { Border } from './border';

export const PopUp: React.FC<PopUpType> = ({
  children,
  height,
  Close,
  isVisible,
}) => {
  const fullWidth = Dimensions.get('window').width;
  const fullHeight = Dimensions.get('window').height;

  return (
    <Box
      position={'absolute'}
      top={0}
      zIndex={2}
      display={isVisible ? 'flex' : 'none'}
      width={'100%'}
      height={'100%'}
      justifyContent={'center'}
      alignItems={'center'}
    >
      <TouchableOpacity style={StyleSheet.absoluteFill} onPress={Close}>
        <Box
          role={'gray12'}
          opacity={0.4}
          position={'absolute'}
          top={0}
          bottom={0}
          left={0}
          right={0}
        />
      </TouchableOpacity>
      <Border radius={8}>
        <Box height={height} width={fullWidth * 0.8} role={'primary100'}>
          <Spacing m={7}>
            <Stack size={5}>
              <Queue justifyContent={'flex-end'}>
                <TouchableOpacity onPress={Close}>
                  <CloseLineIcon width={21} height={21}></CloseLineIcon>
                </TouchableOpacity>
              </Queue>
              <Stack alignItems={'center'} size={8}>
                {children}
              </Stack>
            </Stack>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

type PopUpType = {
  children?: any;
  height?: number | string;
  style?: any;
  Close?: () => void;
  onClick?: () => void;
  isVisible: boolean;
};

import React from 'react';
import { Box, Text, Button, CloseLineIcon } from '../../components';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native';

export const TopNavigation: React.FC<WarningType> = ({ type, name }) => {
  const navigation = useNavigation();

  const BackNavigation = () => {
    return (
      <Box
        flex={1}
        flexDirection="row"
        alignItems="center"
        justifyContent="center"
        role={'accentNest'}
      >
        <Box height={64} width={50}></Box>
        <Box flex={1} justifyContent="center" alignItems="center">
          <Text
            textAlign="center"
            fontFamily="Montserrat"
            type="title3"
            bold={true}
          >
            {name}
          </Text>
        </Box>
        <Box width={50} height={64} justifyContent="center" alignItems="center">
          <Button
            onPress={() => {
              navigation.navigate('MainRoot');
            }}
            category="text"
          >
            <CloseLineIcon />
          </Button>
        </Box>
      </Box>
    );
  };

  return (
    <Box
      width="100%"
      height={64}
      justifyContent="center"
      alignItems="center"
      top={0}
    >
      {type === 'back' ? <BackNavigation /> : <Box />}
    </Box>
  );
};
type WarningType = {
  type: 'back';
  name?: string;
};

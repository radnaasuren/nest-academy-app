import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Border, Box, Spacing, Text, BackgroundImage } from '..';

type cardTypes = {
  source?: any;
  title?: string;
  lesson?: string;
  icon?: any;
  onPress?: Function;
};

export const LessonCard: React.FC<cardTypes> = ({
  source,
  title,
  lesson,
  onPress,
}) => {
  return (
    <Border radius={8}>
      <Box
        width={342}
        height={92}
        role="white"
        top={16}
        justifyContent="center"
        alignItems="center"
      >
        <Box
          width="90%"
          height={80}
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <BackgroundImage source={source} height={64} width={64} />
          <Box flex={1} left={15}>
            <Text type="callout" role="primary500">
              Wire frame
            </Text>
            <Text type="caption2" role="primary400">
              Хичээл 2
            </Text>
          </Box>
          <TouchableOpacity
            onPress={() => {
              onPress ? onPress : null;
            }}
          >
            <Border radius={20}>
              <Box width={40} height={40} role="primary500"></Box>
            </Border>
          </TouchableOpacity>
        </Box>
      </Box>
    </Border>
  );
};

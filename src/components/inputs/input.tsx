import React, { useEffect, useRef, useState } from 'react';
import { Animated, StyleSheet, TextInput } from 'react-native';
import { Border, Text, Box, Spacing } from '..';
import { HelpText, InputMessage } from '.';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { EyeIcon, KeyIcon } from '../icons';
import { useTheme } from '../theme-provider';
import { Stack } from '../layout';

interface Props {
  placeholder: string;
  type?: 'default' | 'password';
  status?: 'default' | 'error' | 'disabled' | 'success';
  LeftIcon?: any;
  value?: string;
  RigthIcon?: any;
  counter?: boolean;
  helperText?: string;
  keyboardType?:
    | 'default'
    | 'number-pad'
    | 'decimal-pad'
    | 'numeric'
    | 'email-address'
    | 'phone-pad';
  messageText?: string;
  messageType?: 'default' | 'error' | 'warning' | 'success';
  onKeyPress?: Function;
  onChangeText?: Function;
  onSubmitEditing?: Function;
  width?: any;
  onFocus?: Function;
}

export const InputAllInOne: React.FC<Props> = (props) => {
  const {
    onChangeText,
    helperText,
    counter,
    messageText,
    messageType,
    ...others
  } = props;
  const [value, setValue] = useState('');
  const expand = messageText && messageType;
  const animationIndex = useRef(new Animated.Value(0)).current;
  const height = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [54, 100],
  });

  useEffect(() => {
    if (expand)
      Animated.timing(animationIndex, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
    else
      Animated.timing(animationIndex, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  }, [messageText, messageType]);

  return (
    <Box width={'auto'} height={'auto'}>
      <Stack size={3}>
        <Animated.View style={{ height }}>
          <Stack size={1}>
            <Input
              onChangeText={(text: string) => {
                onChangeText && onChangeText(text);
                setValue(text);
              }}
              {...others}
            />
            <InputMessage role={messageType}>
              <Text role={'primary500'} type={'subheading'}>
                {messageText}
              </Text>
            </InputMessage>
          </Stack>
        </Animated.View>
        <HelpText
          value={helperText}
          width={'100%'}
          height={18}
          percent={counter ? value.length : undefined}
        />
      </Stack>
    </Box>
  );
};

export const InputWithMessage: React.FC<Props> = (props) => {
  const { messageText, messageType } = props;
  const expand = messageText && messageType;
  const animationIndex = useRef(new Animated.Value(0)).current;
  const height = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [54, 108],
  });

  useEffect(() => {
    if (expand)
      Animated.timing(animationIndex, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  }, [messageText, messageType]);

  return (
    <Box width={'100%'}>
      <Animated.View style={{ height }}>
        <Stack size={1}>
          <Input {...props} />
          <InputMessage role={messageType}>
            <Text role={'primary500'} type={'subheading'}>
              {messageText}
            </Text>
          </InputMessage>
        </Stack>
      </Animated.View>
    </Box>
  );
};

export const InputWithHelperTextAndCounter: React.FC<Props> = (props) => {
  const { onChangeText, helperText, counter, ...others } = props;
  const [value, setValue] = useState('');

  return (
    <Box width={'100%'} height={'auto'}>
      <Input
        width={'100%'}
        onChangeText={(text: string) => {
          onChangeText && onChangeText(text);
          setValue(text);
        }}
        {...others}
      />
      <HelpText
        value={helperText}
        width={'100%'}
        percent={counter ? value.length : undefined}
      />
    </Box>
  );
};

export const Input: React.FC<Props> = (props) => {
  const { colors } = useTheme();
  const {
    type,
    placeholder,
    status = 'default',
    LeftIcon,
    RigthIcon,
    value,
    keyboardType,
    onKeyPress,
    onChangeText,
    onSubmitEditing,
    width,
    onFocus,
  } = props;
  const animationIndex = useRef(new Animated.Value(0)).current;
  const [isInputFocus, setIsInputFocus] = useState(false);
  const [visible, setVisible] = useState(true);
  const translateYLabel = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -12],
  });
  const translateXLabel = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -0.9 * placeholder.length * 2],
  });
  const translateYInput = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 7],
  });
  const scale = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 0.7],
  });

  useEffect(() => {
    if (isInputFocus) {
      Animated.timing(animationIndex, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  }, [isInputFocus]);

  const styles = StyleSheet.create({
    label: {
      position: 'absolute',
      zIndex: 0,
    },

    input: {
      height: 45,
      zIndex: 1,
      marginLeft: type === 'password' ? 12 : 0,
      fontSize: 17,
      fontWeight: '400',
      letterSpacing: 1,
    },
  });

  return (
    <Box
      opacity={status === 'disabled' ? 0.5 : 1}
      height={56}
      width={width ? width : '100%'}
    >
      <Border
        radius={4}
        lineWidth={isInputFocus ? 2 : 1}
        role={
          status === 'error'
            ? 'destructive500'
            : status === 'success'
            ? 'success500'
            : isInputFocus
            ? 'primary500'
            : 'primary400'
        }
      >
        <Box
          justifyContent={'center'}
          role={'primary100'}
          height={52}
          width={'100%'}
        >
          <Spacing ph={4}>
            <Box
              justifyContent={'center'}
              alignItems={'center'}
              flexDirection={'row'}
            >
              <Spacing mr={LeftIcon ? 3 : 0}>
                {type === 'password' ? <KeyIcon /> : LeftIcon && <LeftIcon />}
              </Spacing>
              <Animated.View
                style={[
                  styles.label,
                  { left: type === 'password' ? 34 : LeftIcon ? 30 : 0 },
                  {
                    transform: [
                      { translateY: translateYLabel },
                      { scale },
                      { translateX: translateXLabel },
                    ],
                  },
                ]}
              >
                <Text
                  type={'body'}
                  role={
                    status === 'error'
                      ? 'destructive500'
                      : status === 'success'
                      ? 'success500'
                      : 'primary400'
                  }
                >
                  {placeholder}
                </Text>
              </Animated.View>
              <Animated.View
                style={{
                  width:
                    type === 'password'
                      ? '86%'
                      : LeftIcon || RigthIcon
                      ? '92%'
                      : '100%',
                  transform: [{ translateY: translateYInput }],
                }}
              >
                <TextInput
                  style={[styles.input, { color: colors['primary500'] }]}
                  onFocus={() => {
                    setIsInputFocus(true), onFocus && onFocus();
                  }}
                  onBlur={() => setIsInputFocus(false)}
                  value={value}
                  keyboardType={keyboardType}
                  autoFocus={value ? true : false}
                  editable={status === 'disabled' ? false : true}
                  scrollEnabled={false}
                  secureTextEntry={type === 'password' ? visible : false}
                  onChangeText={(text) => onChangeText && onChangeText(text)}
                  onSubmitEditing={() => onSubmitEditing && onSubmitEditing()}
                  onKeyPress={(e) =>
                    onKeyPress && onKeyPress(e.nativeEvent.key)
                  }
                />
              </Animated.View>
              <TouchableOpacity
                onPress={() => setVisible(!visible)}
                style={{ display: type === 'password' ? 'flex' : 'none' }}
              >
                <EyeIcon />
              </TouchableOpacity>
              {RigthIcon && <RigthIcon />}
            </Box>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

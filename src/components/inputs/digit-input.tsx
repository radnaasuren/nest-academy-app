import React, { useRef, useContext, useEffect, useState } from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native';
import { AuthContext } from '../../provider/auth';
import { Box } from '../layout/box';

const styles = StyleSheet.create({
  digit: {
    width: 40,
    height: 47,
    backgroundColor: '#F2F2F7',
    borderRadius: 4,
    textAlign: 'center',
    marginHorizontal: 10,
  },
  onFocus: {
    borderWidth: 1,
    borderColor: '#172B4D',
    borderStyle: 'solid',
  },
  hidden: {
    display: 'none',
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export const DigitInput: React.FC<any> = ({ reset }) => {
  const { code, setcode } = useContext(AuthContext);
  const [focus, setfocus] = useState(0);
  const [digit, setdigit] = useState()
  let digits = new Array(6).fill(0);
  const input = useRef(null)


  const Manage = (event: any) => {
    if (event.nativeEvent.key == 'Backspace') {
      setdigit({ ...digit, [`pin${code.length - 1}`]: null });
    }
  }


  useEffect(() => {
    if (code) {
      code.split('').map((pin: any, index: any) => {
        setdigit((digit: any) => ({ ...digit, [`pin${index}`]: pin }))
      })
    }
    setfocus(code.length)
    if (!code) {
      setdigit(null)
    }
  }, [code])


  return (
    <Box
      height={56}
      flexDirection="row"
      justifyContent="center"
      alignItems="center"
    >

      <TextInput
        ref={input}
        keyboardType="numeric"
        maxLength={6}
        onChangeText={(code) => {
          setcode(code);
        }}
        onKeyPress={(event: any) => Manage(event)}
        style={styles.hidden}
        value={code}
      />

      {digits.map((value, index) => (
        <TouchableOpacity
          onPress={() => {
            input.current?.focus();
          }}
          key={'digit-' + index}
          style={styles.center}
        >
          <View
            style={[
              styles.digit,
              styles.center,
              focus == index ? styles.onFocus : null,
            ]}
          >
            {digit?.[`pin${index}`] ? (
              <Text style={styles.center}>{digit?.[`pin${index}`]}</Text>
            ) : (
              <Text style={[styles.center, { color: '#E8E8E8', fontSize: 20 }]}>
                0
              </Text>
            )}
          </View>
        </TouchableOpacity>
      ))}

    </Box>
  );
};

import React, { createContext, useEffect, useState } from 'react';
import { KeyboardAvoidingView } from 'react-native';
import { Box } from '..';
import { BottomButtons } from './bottom-buttons';
import { Header } from './header';

interface ProgressStepsProps {
  lastButtonFunction: Function;
  setState?: Function;
  children?: any;
}

export const ProgressStepsContext = createContext<any>(null);

export const ProgressSteps: React.FC<ProgressStepsProps> = ({
  lastButtonFunction,
  children,
  setState,
}) => {
  const childrens: any = React.Children.toArray(children);
  const [currentPageIndex, setCurrentPageIndex] = useState<any>(0);
  const [userInfo, setUserInfo] = useState({
    forWho: '',
    name: '',
    date: '',
    gender: '',
    course: '',
    grade: '',
    school: '',
    email: '',
    phone: '',
    fromWho: '',
    facebook: '',
    educationStatus: '',
    description: '',
  });

  useEffect(() => {
    setState && setState(userInfo);
  }, [userInfo]);

  return (
    <ProgressStepsContext.Provider
      value={{
        childrens,
        currentPageIndex,
        setCurrentPageIndex,
        lastButtonFunction,
        userInfo,
        setUserInfo,
      }}
    >
      <Box width={'100%'} height={'100%'} alignItems={'center'} role={'white'}>
        <Header />
        {childrens[currentPageIndex]}
        <KeyboardAvoidingView
          style={{ width: '100%' }}
          keyboardVerticalOffset={80}
          behavior={'padding'}
        >
          <BottomButtons />
        </KeyboardAvoidingView>
      </Box>
    </ProgressStepsContext.Provider>
  );
};

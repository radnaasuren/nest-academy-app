import React, { useContext } from 'react';
import { Dimensions, View } from 'react-native';
import { ProgressStepsContext } from './progress-steps';
import { Box, Spacing, Button, Text, ArrowIcon } from '..';
import { RightArrowIcon, TrailingIcon } from '../icons';
import { Queue } from '../layout';
const { width } = Dimensions.get('window');

interface getScreenNameProps {
  array: any;
  index: number;
  jumpTo: 'next' | 'previous';
}

export const BottomButtons: React.FC<any> = () => {
  const {
    childrens,
    currentPageIndex,
    setCurrentPageIndex,
    lastButtonFunction,
  } = useContext(ProgressStepsContext);

  const onClick = async ({ array, index, jumpTo }: getScreenNameProps) => {
    const length = array.length - 1;
    const { onNext, catchFunction } = array[index].props;

    switch (jumpTo) {
      case 'next':
        if (index === length)
          try {
            await lastButtonFunction();
          } catch (error) {
            catchFunction && catchFunction(error);
          }
        else {
          try {
            (await onNext) && (await onNext());
            await setCurrentPageIndex(index + 1);
          } catch (error) {
            catchFunction && (await catchFunction(error));
          }
        }
        break;

      case 'previous':
        if (index !== 0) setCurrentPageIndex(index - 1);
        break;
    }
  };

  if (currentPageIndex === 0)
    return (
      <Spacing mh={4} mb={8} grow={1}>
        <Box width={'100%'} role={'white'}>
          <Button
            onPress={() =>
              onClick({
                array: childrens,
                index: currentPageIndex,
                jumpTo: 'next',
              })
            }
            category={'fill'}
            size={'l'}
            width={'100%'}
          >
            <Box flexDirection={'row'} alignItems={'center'}>
              <Text
                type={'body'}
                fontFamily={'Montserrat'}
                bold
                role={'white'}
                width={'auto'}
              >
                ДАРААХ
              </Text>
              <TrailingIcon height={30} width={30} />
            </Box>
          </Button>
        </Box>
      </Spacing>
    );

  return (
    <Spacing mh={4} mb={8} grow={1}>
      <Queue
        size={2}
        width={'100%'}
        justifyContent={'space-between'}
        role={'white'}
        alignItems={'center'}
      >
        <Box flex={1}>
          <Button
            onPress={() =>
              onClick({
                array: childrens,
                index: currentPageIndex,
                jumpTo: 'previous',
              })
            }
            category={'ghost'}
            size={'l'}
            width={'100%'}
          >
            <Text
              type={'callout'}
              fontFamily={'Montserrat'}
              bold
              role={'primary500'}
            >
              ӨМНӨХ
            </Text>
          </Button>
        </Box>
        <Box flex={1}>
          <Button
            onPress={() =>
              onClick({
                array: childrens,
                index: currentPageIndex,
                jumpTo: 'next',
              })
            }
            category={'fill'}
            size={'l'}
            width={'100%'}
          >
            <Box flexDirection={'row'} alignItems={'center'}>
              <Text
                type={'callout'}
                fontFamily={'Montserrat'}
                bold
                role={'white'}
                width={'auto'}
              >
                ДАРААХ
              </Text>
              <TrailingIcon height={30} width={30} />
            </Box>
          </Button>
        </Box>
      </Queue>
    </Spacing>
  );
};

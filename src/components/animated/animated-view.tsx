import React, { useEffect, useRef, FC } from 'react';
import { Animated } from 'react-native';
import { useTheme } from '../theme-provider';
import { ColorType } from '../types';

export const AnimatedView: FC<DynamicViewType> = ({
  position,
  children,
  role,
  width,
  height,
  changeX = 0,
  changeY = 0,
  changeO = 1,
  duration = 1000,
  start = true,
}) => {
  const { colors } = useTheme();
  const LocationX = useRef(new Animated.Value(0)).current;
  const LocationY = useRef(new Animated.Value(0)).current;
  const Opacity = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    start
      ? Animated.parallel([
          Animated.timing(LocationX, {
            duration,
            useNativeDriver: false,
            toValue: changeX,
          }),
          Animated.timing(LocationY, {
            duration,
            useNativeDriver: false,
            toValue: changeY,
          }),
          Animated.timing(Opacity, {
            duration,
            useNativeDriver: false,
            toValue: changeO,
          }),
        ]).start()
      : 0;
  }, [start]);

  return (
    <Animated.View
      style={[
        {
          width,
          height,
          backgroundColor: role ? colors[role] : 'transparent',
          position,
        },
        {
          transform: [
            {
              translateX: LocationX,
            },
            {
              translateY: LocationY,
            },
          ],
        },
        {
          opacity: Opacity,
        },
      ]}
    >
      {children}
    </Animated.View>
  );
};

type DynamicViewType = {
  position?: 'absolute' | 'relative';
  changeX?: number;
  changeY?: number;
  changeO?: number;
  role?: ColorType;
  width?: string | number;
  height?: number | string;
  duration?: number;
  children?: any;
  start?: boolean;
};

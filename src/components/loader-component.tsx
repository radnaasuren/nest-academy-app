import React from 'react';
import { Border } from './core';
import { Box } from './layout';

interface Props {
   height: number | string,
   width: number | string,
}

export const LoaderComponent: React.FC<Props> = ({ height, width }) => {

   return (
      <Border radius={8}>
         <Box width={width} height={height} role={'gray'} />
      </Border>
   )
}
import React from 'react';
import { Box, Spacing } from './layout/index';
import { Text, Border } from './core/index';
import { ColorType } from '../components/types';

export const TimeSchedule: React.FC<ProgressType> = ({
  title = 'Гарчиг',
  children,
  from,
  to,
}) => {
  return (
    <Border radius={4}>
      <Box role={'white'}>
        <Spacing p={4} grow={1}>
          <Box flex={1} flexDirection={'row'} alignItems={'center'}>
            <Box alignItems={'center'}>
              <Spacing mb={2}>
                <Text bold role={'primary500'} type={'callout'}>
                  {from}
                </Text>
              </Spacing>
              <Box role={'primary300'} flex={1} width={2} />
              <Spacing mt={2}>
                <Text bold role={'primary500'} type={'callout'}>
                  {to}
                </Text>
              </Spacing>
            </Box>
            <Box flex={1}>
              <Spacing ml={4.5}>
                <Spacing mb={2}>
                  <Text bold role={'primary500'} type={'body'}>
                    {title}
                  </Text>
                </Spacing>
                <Text role={'black100'} width={'auto'}>
                  {children}
                </Text>
              </Spacing>
            </Box>
          </Box>
        </Spacing>
      </Box>
    </Border>
  );
};

type ProgressType = {
  title?: string;
  width?: number | string;
  height?: number | string;
  role?: ColorType;
  backgroundRole?: ColorType;
  children?: JSX.Element | JSX.Element[] | string;
  from?: string;
  to?: string;
};

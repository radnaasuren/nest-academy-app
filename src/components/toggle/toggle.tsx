import React, { useEffect, useRef, useState } from 'react';
import { TouchableOpacity, Dimensions, Animated } from 'react-native';
import { Border, Box, Text } from '..';
import _ from 'lodash';

const getWidth = (Dimensions.get('window').width - 32) / 2;

export const Toggle: React.FC<any> = ({ children, sessions }) => {
  const [toggle, setToggle] = useState(true);
  const child: any = React.Children.toArray(children);
  const xValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.spring(xValue, {
      toValue: toggle ? 0 : 1,
      velocity: 0.1,
      useNativeDriver: true,
    }).start();
  }, [toggle]);

  const widthAnim = xValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, getWidth],
  })

  return (
    <>
      {child.length == 2 &&
        <TouchableOpacity onPress={() => setToggle((toggle) => !toggle)}>
          <Border lineWidth={2} role={'primary500'} radius={8}>
            <Box
              height={42}
              width={'100%'}
              flexDirection="row"
              zIndex={1}
              alignItems={'center'}
            >
              <Box height={42} width={'100%'} zIndex={0} position={'absolute'}>
                <Box height={42} width={'100%'} role="white" position={'absolute'} />
                <Animated.View
                  style={[
                    {
                      borderRadius: 6,
                      height: '100%',
                      width: '50%',
                      backgroundColor: '#172B4D',
                      position: 'absolute',
                      zIndex: 0,
                    },
                    {
                      transform: [
                        {
                          translateX: widthAnim,
                        },
                      ],
                    },
                  ]}
                />
                <Box left={0} width={'50%'} height={'100%'} justifyContent={"center"} alignItems={'center'} position={'absolute'} zIndex={1}>
                  <Text bold role={toggle ? 'white' : 'primary500'}>{sessions[0]}</Text>
                </Box>
                <Box right={0} width={'50%'} height={'100%'} justifyContent={"center"} alignItems={'center'} position={'absolute'} zIndex={1}>
                  <Text bold role={toggle ? 'primary500' : 'white'}>{sessions[1]}</Text>
                </Box>
              </Box>
            </Box>
          </Border>
        </TouchableOpacity>
      }
      <Box height={'auto'} width={'auto'}>
        {
          toggle ? <>{_.head(child)}</> : <>{_.tail(child)}</>
        }
      </Box>
    </>
  )
};

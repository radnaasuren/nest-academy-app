import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const CheckIcon: React.FC<IconType> = ({
  height = 28,
  width = 28,
  role = 'success500',
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 28 28" fill="none">
      <Path
        d="M14 27.334C6.634 27.334.665 21.364.665 14S6.636.667 13.999.667c7.364 0 13.334 5.97 13.334 13.333 0 7.364-5.97 13.334-13.334 13.334zm-1.33-8l9.427-9.428L20.21 8.02l-7.541 7.543-3.772-3.772-1.885 1.885 5.657 5.658z"
        fill={colors[role]}
      />
    </Svg>
  );
};

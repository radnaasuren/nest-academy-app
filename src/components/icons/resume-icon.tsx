import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const ResumeIcon: React.FC<IconType> = ({ height = 26, width = 29 }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 26 29" fill="none">
      <Path d="M26 14.5L0 29V0l26 14.5z" fill="#FCFCFF" />
    </Svg>
  );
};

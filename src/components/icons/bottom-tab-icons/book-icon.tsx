import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../../theme-provider';
import { IconType } from '../../types';

export const BookIcon: React.FC<IconType> = ({
  role = 'black',
  width = 24,
  height = 24,
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none">
      <Path
        d="M4 19.5A2.5 2.5 0 016.5 17H20"
        stroke={colors[role]}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M6.5 2H20v20H6.5A2.5 2.5 0 014 19.5v-15A2.5 2.5 0 016.5 2v0z"
        stroke={colors[role]}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};

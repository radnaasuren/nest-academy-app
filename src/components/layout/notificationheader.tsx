import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from '../core';
import { LeftArrowIcon } from '../icons';
import { Box } from './box';
import { Queue } from './queue';

export const NotifcationHeader: React.FC<any> = () => {
  const navigation = useNavigation();
  return (
    <Box alignItems={'center'} flexDirection={'row'} justifyContent="center">
      <Box position={'absolute'} left={10}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <LeftArrowIcon />
        </TouchableOpacity>
      </Box>
      <Box>
        <Text type={'body'} fontFamily={'Montserrat'} bold>
          Notifcation
        </Text>
      </Box>
    </Box>
  );
};

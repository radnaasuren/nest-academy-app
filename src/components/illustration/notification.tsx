import React from 'react';
import {
  Svg,
  Path,
  Mask,
  Defs,
  G,
  LinearGradient,
  Stop,
} from 'react-native-svg';

interface Props {
  width?: number | string;
  height?: number | string;
}

export const Notififcation: React.FC<Props> = ({
  width = 160,
  height = 160,
  ...others
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 160 159" fill="none">
      <Path
        d="M69.362 12.167L21.984 30.806C3.46 38.094-3.888 56.003 3.714 75.352l17.547 44.654c7.58 19.293 25.151 27.4 43.676 20.112l47.378-18.639c18.525-7.287 25.818-25.175 18.237-44.47l-17.546-44.653c-7.603-19.348-25.12-27.476-43.644-20.189z"
        fill="url(#prefix__paint0_linear)"
      />
      <G>
        <Mask
          id="prefix__a"
          x={40.578}
          y={39.529}
          width={120}
          height={120}
          fill="#000"
        >
          <Path fill="#fff" d="M40.578 39.529h120v120h-120z" />
          <Path d="M125.77 40.53H74.867c-19.903 0-33.289 13.978-33.289 34.77v47.987c0 20.734 13.386 34.713 33.29 34.713h50.902c19.903 0 33.23-13.979 33.23-34.713V75.301c0-20.793-13.327-34.772-33.23-34.772z" />
        </Mask>
        <Path
          d="M125.77 40.53H74.867c-19.903 0-33.289 13.978-33.289 34.77v47.987c0 20.734 13.386 34.713 33.29 34.713h50.902c19.903 0 33.23-13.979 33.23-34.713V75.301c0-20.793-13.327-34.772-33.23-34.772z"
          fill="#00DCF0"
        />
        <Path
          d="M125.77 41.53a1 1 0 000-2v2zm0-2a1 1 0 100 2v-2zm0 0H74.867v2h50.903v-2zm-50.903 0c-10.194 0-18.788 3.585-24.83 9.896-6.04 6.308-9.459 15.262-9.459 25.875h2c0-10.18 3.274-18.612 8.903-24.492 5.627-5.876 13.678-9.28 23.386-9.28v-2zM40.578 75.3v47.987h2V75.301h-2zm0 47.987c0 10.585 3.42 19.524 9.46 25.824C56.08 155.414 64.672 159 74.867 159v-2c-9.71 0-17.76-3.404-23.387-9.273-5.63-5.873-8.903-14.29-8.903-24.44h-2zM74.868 159h50.902v-2H74.867v2zm50.902 0c10.194 0 18.773-3.586 24.801-9.89 6.025-6.3 9.429-15.24 9.429-25.823h-2c0 10.151-3.259 18.568-8.874 24.441-5.612 5.868-13.648 9.272-23.356 9.272v2zM160 123.287V75.301h-2v47.986h2zm0-47.986c0-10.612-3.404-19.566-9.428-25.874-6.029-6.312-14.608-9.898-24.802-9.898v2c9.708 0 17.744 3.404 23.355 9.28C154.74 56.689 158 65.118 158 75.3h2z"
          fill="url(#prefix__paint1_linear)"
          mask="url(#prefix__a)"
        />
      </G>
      <Path
        d="M103.748 118.5a4.323 4.323 0 01-3.748 2.159 4.332 4.332 0 01-3.748-2.159m25.415-8.667H78.333a6.5 6.5 0 006.5-6.5V92.5a15.167 15.167 0 1130.334 0v10.833a6.502 6.502 0 006.5 6.5z"
        stroke="#fff"
        strokeWidth={6}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Defs>
        <LinearGradient
          id="prefix__paint0_linear"
          x1={100.291}
          y1={0}
          x2={58.655}
          y2={120.459}
          gradientUnits="userSpaceOnUse"
        >
          <Stop stopColor="#00DCF0" />
          <Stop offset={1} stopColor="#00B4C5" />
        </LinearGradient>
        <LinearGradient
          id="prefix__paint1_linear"
          x1={60.27}
          y1={54.215}
          x2={135.21}
          y2={146.992}
          gradientUnits="userSpaceOnUse"
        >
          <Stop stopColor="#fff" stopOpacity={0.25} />
          <Stop offset={1} stopColor="#fff" stopOpacity={0} />
        </LinearGradient>
      </Defs>
    </Svg>
  );
};

import { ArrowIcon } from './icons';
import { Text, Spacing } from '../components';
import React, { useEffect, useState } from 'react';
import {
  TouchableOpacity,
  LayoutAnimation,
  Platform,
  UIManager,
} from 'react-native';
import { VideoPlayer } from './video-player';
import { Box } from './layout';
import { Border } from './core';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

export const Collapse: React.FC<CollapseType> = ({
  title,
  body,
  url,
  index,
  indexOfOpenedQuestion,
  setIndexOfOpenedQuestion,
}) => {
  const [open, setopen] = useState(false);

  useEffect(() => {
    if (indexOfOpenedQuestion !== null)
      setopen(indexOfOpenedQuestion === index ? true : false);
  }, [indexOfOpenedQuestion]);

  const onPress = () => {
    if (open) setIndexOfOpenedQuestion && setIndexOfOpenedQuestion(null);
    else setIndexOfOpenedQuestion && setIndexOfOpenedQuestion(index);

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setopen(!open);
  };

  return (
    <TouchableOpacity
      style={[!open && open && { justifyContent: 'center', height: 56 }]}
      onPress={onPress}
      activeOpacity={1}
    >
      <Border lineWidth={1} radius={8} role={'lightgray'}>
        <Box flex={1} justifyContent={'center'} overflow={'hidden'}>
          <Spacing p={4}>
            <Box
              flex={1}
              flexDirection={'row'}
              justifyContent={'space-between'}
              alignItems={'center'}
            >
              <Text fontFamily={'Montserrat'}>{title}</Text>
              <ArrowIcon></ArrowIcon>
            </Box>
          </Spacing>
          <Box
            flex={1}
            alignItems={'center'}
            display={open ? 'flex' : 'none'}
            role={'lightgray'}
          >
            <Spacing mt={4}>
              <VideoPlayer
                repeat={true}
                source={{ uri: url }}
                width={311}
                height={250}
                pause={!open}
              />
            </Spacing>
            <Spacing p={4}>
              <Box height={10} />
              <Text fontFamily={'Montserrat'}>{body}</Text>
            </Spacing>
          </Box>
        </Box>
      </Border>
    </TouchableOpacity>
  );
};

type CollapseType = {
  title?: JSX.Element | JSX.Element[] | string;
  body?: JSX.Element | JSX.Element[] | string;
  indexOfOpenedQuestion?: number | null;
  setIndexOfOpenedQuestion?: Function;
  url?: string | undefined;
  index?: number;
};
